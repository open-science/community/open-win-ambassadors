## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 04 July 2023, 10:00-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/r1r50HZt3/edit)
- Last meeting notes: [27 June 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/H15EPzdO3)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Peter Doohan/ he/him / NDCN / @vyp730 / 0000-0003-2122-7354 
5. Juju Fars / they/them / NDCN / @julienfars / 0000-0001-7771-5029
6. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World

#### A) WIN Monday messages
- [Peter's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/newsletter/open%20science%20bites.md)
- new contact (since Andrew left): win-admin@ndcn.ox.ac.uk


#### B) Twitter
- [Juju's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/social_comm.md)


### Git/Gitlab training
- group discussion next week?


### Electrophysiology database
- Peter to move things to gitlab pages soon
- IBL (international brain lab) projects as a BIDS equivalent (?)


### Gitlab issues and community pages
- <mark>*everyone*: if there is anything out of date, please raise an issue on gitlab (or update community pages directly)</mark>


### Updated gitlab issues
- https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues


### Capturing open research at WIN
- Juju to draft an email / poll to gage interest in a common repository of posters


<br>

## Next Actions
- [ ] [project templates](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/projects)
- [ ] continuing work on group and individual projects
- [ ] next meeting: 11 July 2023 --> group work on git/gitlab course

    

<br>

---

## Feedback on this session
*Please add a few words to each of the sections below so we can improve the experience of everyone.*
### What worked well?

### What surprised you?

### What would you change?

### What would you like to know more about?
