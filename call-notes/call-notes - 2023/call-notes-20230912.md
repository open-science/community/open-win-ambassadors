## Agenda and collaborative notes

-----

**General information**

- Date/time: 12 Sep 2023, 10:00-11:00 (BST)
- Location: remote
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/S1UnboaA3/edit)
- Last meeting notes: [05 Sep 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/Bya7p8EA2/edit)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Juju Fars / they/them / NDCN / @pfr545 / 0000-0001-7771-5029
2. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789
3. Yingshi Feng / she/her / NDCN / @yingshif / 0000-0001-9065-4945

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World

#### A) WIN Monday messages
- [Peter's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/newsletter/open%20science%20bites.md)


#### B) Twitter
- [Juju's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/social_comm.md)
    - in coordination with Carinne and Hannah from PE team to be sent out 

#### C) Poster repository 
- [email draft](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/draft_email.md)
Please add/modify if needed. Should be sent when people get back from holidays. 
- get feedback and send out soon
---

## Git/Gitlab training

### <mark>Updates</mark>

GitLab repository to collect all notes, material, presentation slides, etc.: [git course repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/GitLab%20training)


<*please add things below*>

1. [Peter] Finalise structure: write a proposed outline with timings

2. [Juju] organising logistics (find a date, room booking, coordinating with admin, etc
    - git course as part of a reboot camp: 
    - up to 3h in  (e.g. 1h presentation, 2h exercises)
    - Jan 15-19 or April 22-26 (probably the better time, more coding heavy)

3. [Yingshi] prerequisites (how to get a gitlab account, etc.)

4. [Mohamed] checking that [git tutorials](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/gitlab-tutorials/) on community pages are up to date

5. [Verena] High-level intro
- [First draft of slides](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/GitLab%20training/Intro_to_gitlab.pptx)
Please feel free to edit and add to the slides.

6. [Peter, Lisa] preparing slides for github desktop

7. [Miguel] preparing slides on gitlab browser interface

8. creating (worked-through) examples and exercises

9. testing exercises (and trouble-shooting)

12. other ...


<br>

## Other project updates
- <mark>Oct 09th</mark> (intro week for grad course):
    - 15min general introduction to open science and what we are doing at WIN 
    - Juju in touch with Camila 
- idea: maybe do a WIP in October on git and promote ambassadors programme


### Bugs
- open-win email account: Cass's email has been removed
- WIP repo: issue still not resovled with hosting (transferring Cass's private repo)
- Open WIN website is getting out of date
    - e.g. Atom 

<br>

---

## Next Actions
- [ ] next meeting: 19 Sep 2023
- [ ] WIN management meeting (18 Sep)
    - any agenda items that should be discussed?
        - maybe every month highlight the current projects of various groups (PE, EDI, OS) to increase knowledge/visibility of what is going on and what groups are planning
    - <mark> everyone please provide updates on current state of projects and plans for next couple of months by next week </mark>
 

<br>

---

## Feedback on this session
*Please add a few words to each of the sections below so we can improve the experience of everyone.*
### What worked well?

### What surprised you?

### What would you change?

### What would you like to know more about?
