## Agenda and collaborative notes

-----

**General information**

- Date/time: 05 Sep 2023, 09:30-10:30 (BST)
- Location: remote
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/Bya7p8EA2/edit)
- Last meeting notes: [11 July 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/BJzhyFBY3)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Lisa Spiering / she/her / Exp Psychology / @wym003 / 0000-0002-9071-6541
2. Mohamed Tachrount/ he/him / NDCN / @mtach / 0000-0003-1147-3412
3. Juju Fars / they/them / NDCN / @pfr545 / 0000-0001-7771-5029
4. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789
5. Verena Sarrazin/ she/her / Psychiatry / @verenasarrazin / 0000-0002-5796-5378

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World

#### A) WIN Monday messages
- [Peter's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/newsletter/open%20science%20bites.md)


#### B) Twitter
- [Juju's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/social_comm.md)
- everyone to give feedback by end of the week!
- Send a message to the PE team 
#### C) Poster repository 
- [email draft](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/draft_email.md)
Please add/modify if needed. Should be sent when people get back from holidays. 
- get feedback and send out soon
---

## Git/Gitlab training

### <mark>Updates</mark>

GitLab repository to collect all notes, material, presentation slides, etc.: [git course repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/GitLab%20training)


<*please add things below*>

1. [Peter] Finalise structure: write a proposed outline with timings

2. [Juju] organising logistics (find a date, room booking, coordinating with admin, etc

3. [Yingshi] prerequisites (how to get a gitlab account, etc.)

4. [Mohamed] checking that [git tutorials](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/gitlab-tutorials/) on community pages are up to date

5. [Verena] High-level intro
- [First draft of slides](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/GitLab%20training/Intro_to_gitlab.pptx)
Please feel free to edit and add to the slides.

6. [Peter, Lisa] preparing slides for github desktop

7. [Miguel] preparing slides on gitlab browser interface

8. creating (worked-through) examples and exercises

9. testing exercises (and trouble-shooting)

12. other ...


<br>

## Other project updates

- Lisa to take up an internship for next 3 months


### Bugs
- open-win email account: Cass's email has expired and bounces --> raised an issue with central IT
- WIP repo: issue still not resovled with hosting (transferring Cass's private repo)
- Open WIN website is getting out of date
    - e.g. Atom 

### Open science celebration dinner 
- planned for some day in January 
- similar format to last year's
- will probably include PE ambassadors as well

### Oxford open science autumn school
- Nov 20-24
- [info and application](https://ox.ukrn.org/events/#2023-11-20_oxford-berlin-school2023)
- may have the opportunity to include our git training course as a session (to be confirmed)

<br>

---

## Next Actions
- [ ] next meeting: 12 Sep 2023
- [ ] WIN newsletter going out in a couple of weeks:
    - *from Stuart*: "We don’t have to have anything on open science at the moment, but I wanted to offer the space if there is anything that any of the ambassadors wanted to write. This can either be talking about what they have been doing or a pointer to external resources or new open science initiatives."
- [ ] WIN management meeting on 18 Sep 2023:
    - [ ] any agenda items that should be discussed?
    - [ ] <mark> everyone please provide updates on current state of projects and plans for next couple of months by next week </mark>

<br>

---

## Feedback on this session
*Please add a few words to each of the sections below so we can improve the experience of everyone.*
### What worked well?

### What surprised you?

### What would you change?

### What would you like to know more about?
