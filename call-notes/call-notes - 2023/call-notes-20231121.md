# Open WIN Ambassadors meeting - 21 Nov 2023

## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 21 Nov 2023, 10:00-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HJOhE6eEp/edit)
- Last meeting notes: [14 Nov 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/Syl7jWBzp/edit)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Peter Doohan/ he/him / NDCN / @vyp730 / 0000-0003-2122-7354 
5. Juju Fars / they/them / NDCN / @pfr545 / 0000-0001-7771-5029
6. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789
7. Yingshi Feng / she/her / NDCN / @yingshif / 0000-0001-9065-4945

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World

#### A) WIN Monday messages
- [Peter's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/newsletter/open%20science%20bites.md)
    - start sending out again

#### B) Twitter (X)
- [Juju's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/social_comm.md)
- twitter handle: [@OxfordWIN](https://twitter.com/OxfordWIN) ... please repost if on Twitter
    - [latest tweet](https://twitter.com/OxfordWIN/status/1724079837256483264)

#### C) Poster repository !!Pending!! 
- open question of where to host the repo (community pages or WIN pages) 
    - waiting for Stuart to reply 
- TODO: 
    - create a repo on the community or main WIN pages
    - populate with posters from our groups
    - write a short guide on how to use the repo

#### D) Flyers
- [final version](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/28) 
- where would be the best place to put flyers?
    - have put one in the Annexe and will put another one in the Main FMRIB building (Juju)
- ask Stuart for permission?
    - no answers yet

---

## Git/Gitlab training course

### ==Updates==

GitLab repository to collect all notes, material, presentation slides, etc.: 
- [git course repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/GitLab%20training)
- see [issue #18](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/18)


1. [Peter] Finalise structure: write a proposed outline with timings

2. [Juju] organising logistics (find a date, room booking, coordinating with admin, etc

    - git course as part of a reboot camp: 
    - up to 3h in  (e.g. 1h presentation, 2h exercises?)
    - Jan 15-19 or April 22-26 (probably the better time, more coding heavy)
    - contact: Kamila Szulc-Lerch
    - course materials from Paul McCarthy on the Git course repository (if you use some of it, please agknowledge Paul) [Paul's Course](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/GitLab%20training/Git_course_PaulMcCarthy.pptx).

3. [Yingshi] prerequisites (how to get a gitlab account, etc.)
    - [google slides](https://docs.google.com/presentation/d/1Llg5J_1l2wwp6lbN9zztmvLX-RaHUvRPn9bUxUoHVt0/edit?usp=sharing)

4. [Mohamed] checking that [git tutorials](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/gitlab-tutorials/) on community pages are up to date

5. [Verena] High-level intro
    - [google slides](https://docs.google.com/presentation/d/1rLb2jRiYBTtYTN5pp79raAP4igegqOc8wnUQ32xrF1s/edit?usp=sharing)

6. [Peter, Lisa] preparing slides for github desktop

7. [Verena] preparing slides on gitlab browser interface

8. [Verena, Juju] creating (worked-through) examples and exercises
    - idea: maybe combine with other python coding during boot camp?

9. testing exercises (and trouble-shooting)

10. other ...

### Creating a more detailed resources online
- building on existing pages
- adding more advanced information
- develop in parallel to git course
- iterate on slack to work collaboratively

### Timeline
- first draft: ??
- git workshop at Oxford-Berlin autumn school: 21 Nov 2023, 3-5pm
    - lecture and workshop materials: [OSF repo](https://osf.io/5a38t/)
    - workshop slides on our gitlab: [here](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/GitLab%20training/OxBer2023_workshop_GIT_slides.pdf)
- training for new ambassadors: Dec 2023

<br>

## Other updates

- WIN away day (30 Nov) -- meet up during break?
- no meeting next week

### protocols database
- TODO: 
- planning a meeting with Dave Flitney and Duncan Smith (and Stuart?) to discuss..
    - current experiences
    - future steps
    - potential improvements
- potential dates:
    - 07 Dec
    - 12 Dec
    - 14 Dec
    -  ??
- Will create a calendly account and propose dates (Juju)
- will send another email (ask for the 7th in priority and if nothing available --> week after)

### Ambassadors workshop & dinner
- 24th Jan 2024 (afternoon & evening)
- speaker suggestions?
- other open science contacts to invite?


<br>

### Bugs & Issues
- ==!!== [list of open issues on gitlab](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues) ==!!==

<br>

## Next Actions
- [ ] continuing work on group and individual projects
- [ ] next group meeting: 05 Dec 

<br>

---

## Feedback on this session
*Please add any comments, suggestions, ideas, etc. below so that we can improve the experience of everyone.*

