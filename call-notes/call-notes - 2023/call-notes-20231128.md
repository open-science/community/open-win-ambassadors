# Open WIN Ambassadors meeting - 28 Nov 2023

## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 28 Nov 2023, 09:30-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rJ72ZkGST/edit)
- Last meeting notes: 

-----

## Agenda
0. Participation guidelines 
1. Introductions
2. Overview of ambassadors programme
3. Admin & logistics
4. Discussion
5. Actions

## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     /          /            / @         / 0000-
3. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
5. Bernd Taschler / he/him   / BDI        / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----
## 1. Welcome intro



<br>

## 2. Ambassadors programme - Overview

### A little history
- 2017: as part of the WIN bid, one of WIN's themes would be open imaging.
- A lot of people came to open science through the reproducibility crisis. 
- Translational value - why do we never get from clinical research to clinical practice?
- FSL has always been open and free to academic community.
- As a (neuroimaging) community we have always been very open with each other.
- Challenges: started with the technical challenges, but the ethical, governance, skills challenges are increasing recognised as significant
- All 15 WIN PIs agreed to share outputs and "document how we got there".
- ==Vision Statement==: WIN will become an open science community with a positive culture for sharing data, tasks and tools, to improve transparency, reproducibility and impact ...
- Cass hiring represented a shift in the emphasis into the "deeper levels of the iceberg".
- First ambassadors cohort: 2022
- As of 2023: some uncertainty about future (esp. funding), management very committed to keeping open science initiatives.

### Goals
- Output based: putting research outputs into a structure for others to access
- Project based: data can be selectively accessed to fit a requirement of a new project (e.g. participant characteristic).
- Sharing across different levels: setting up the infrastructure to enable everyone at WIN to share publicly when you are ready to do so.
- What do we ned to do to support people to share their pre-processed data? We haven't got any conventions or recommendations for how this should happen.
- How to incentivise open science best practices?
    - good for you
    - funders
    - university schemes (DORA)

### Open science management team
- Clare MacKay (2017-2022) 
- Tom Nichols (2023-)
- Stuart Clare
- Heidi Johansen-Berg
- Duncan Mortimer

### Projects
- community pages
- MR protocols database
- outreach
    - WIN Monday messages, twitter, training, talks, flyers, etc.
- visibility
    - open science in research projects across WIN
- individual projects

<br>

## 3. Admin
- meeting time and dates
- hackmd 
- (GitHub?)
- Git and GitLab
    - Does everyone have a Gitlab account?
    - [info on intranet](https://www.win.ox.ac.uk/research/it/i-want-to/programming/git)
- slack channel
- photos and blurb for community pages
- open@win.ox.ac.uk email
- away day meet-up: 30 Nov, 10:15 or 11:15
- workshop and dinner: 24 Jan


<br>

## 4. Discussion




<br>

## 5. Next Actions
- [ ] next meeting: 05 Dec 2023, 10am



<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*
