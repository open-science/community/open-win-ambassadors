# Open WIN Ambassadors - 16th January 2023
## Agenda and collaborative notes

-----

**Important information**

- Date/time: Monday 16th January 2023, 10:00-11:30 (GMT/UTC)
- Join the call: MS Teams
- These notes: [https://hackmd.io/@FE_yEojCQPW4PD8RcTWR0A/BJ43dcGos/edit](https://hackmd.io/@FE_yEojCQPW4PD8RcTWR0A/BJ43dcGos/edit)
- Call recording: [Shared with Ambassadors only, requires SSO](https://unioxfordnexus-my.sharepoint.com/:v:/g/personal/psyc1182_ox_ac_uk/EWN-dk93Pr5HnF9OAJNt1vcBWUqRrHaGdlY1yyiEyEkNPw?e=MpHuee)



<mark>Yellow = action</mark>


-----

## Agenda
0. Participation guidelines and check-in
1. Tour of tools
2. Discussion of your proposals
3. Celebration meeting
4. Feedback

## Participants
(name / Pronouns / Department / git handles (GitLab;GitHub) / ORCID ID)
1. Cassandra Gould van Praag / she/her / Psychiatry / @cassag;@cassgvp / 0000-0002-8584-4637
2. Lisa
3. Peter
4. Miguel

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect eachother as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

-----

## 1. Tour of tools
- Cass demonstrated each of the open WIN tools which we have developed and discussed some of the pain points.

### Protocols db
- [Community pages: Protocols](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/protocols/) (with user guide) and link to database.

### Tasks repository
- [Community pages: Tasks](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tasks/) with link to gitlab repo

### Reproducible analysis
- [Community pages: Analysis](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/analysis/)

### Open Data Portal
- [Community pages: Data](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/)


## 2. Celebration meeting
- Guests:
    - Ambassadors 2021
    - Ambassadors 2022
    - Tom Nichols, Stewart Claire
    - Sarah Callaghan
    - Sarah Stewart
    - Jackie Thompson
    - James Lee
    - Duncan Smith

- Ambassador's lightning talks:
    - 3 minutes (3 slides) on something open sciencey you're proud of.


## 4. Feedback on this session
<mark>Please add a few words to each of the sections below so we can improve the experience of others.</mark>
### What worked well?
-
### What surprised you?
-
### What would you change?
-
### What would you like to know more about?
-
