## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 11 July 2023, 10:00-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/BJzhyFBY3/edit)
- Last meeting notes: [04 July 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/r1r50HZt3/edit)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Peter Doohan/ he/him / NDCN / @vyp730 / 0000-0003-2122-7354 
2. Lisa Spiering / she/her / Exp Psychology / @wym003 / 0000-0002-9071-6541
3. Miguel Farinha/ he/him / @bpz864 / 0000-0002-1729-723X
4. Mohamed Tachrount/ he/him / NDCN / @mtach / 0000-0003-1147-3412
5. Juju Fars / they/them / NDCN / @pfr545 / 0000-0001-7771-5029
6. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789
7. Yingshi Feng / she/her / NDCN / @yingshif / 0000-0001-9065-4945
8. Verena Sarrazin/ she/her / Psychiatry / @verenasarrazin / 0000-0002-5796-5378

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World

#### A) WIN Monday messages
- [Peter's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/newsletter/open%20science%20bites.md)


#### B) Twitter
- [Juju's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/social_comm.md)


### Gitlab issues and community pages
- *everyone*: if there is anything out of date (e.g. broken links), please raise an issue on gitlab (or update community pages directly)


----

## <mark>Git/Gitlab training</mark>

#### What resources are out there?
- [community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/)
- [Paul's slides](https://docs.google.com/presentation/d/1urIVhNd6W_jH6JkDjwh0vTMNUZ6IZn03iGssUTjLrAA/edit?usp=sharing)
- [Cass's tutorials](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/gitlab-tutorials/)
- [carpentry tutorials](https://software-carpentry.org/lessons/)
- [brainhack tutorials (github)](https://psy6983.brainhackmtl.org/modules/git_github/)
- <*please add*>



#### What did you find difficult when first using git/gitlab?
- <*please add*>



#### What do you (still) find difficult or unintuitive about git/gitlab?
- <*please add*>


#### What should a beginners course cover?
- what is git? why use it?
- what is the difference between git and gitlab? (and github?)
- what is a repository? (and what should it contain?)
- how to use the web interface
- how to use the command line
- basic git commands
- basics about markdown syntax (?)
- how to create an issue on gitlab
- how to clone or fork a repo
- branches
- how to create a merge request
- how to resolve a merge conflict
- (how to use git in VS code) (?)
- <*please add*>


#### What should a more advanced course cover?
- git staging area
- advanced git commands (e.g. git rebase, etc.)
- continuous integration on gitlab
- DOI and licencing
- <*please add*>


#### What would you be able to help with?
1. Finalise structure 
    - write a proposed outline with timings --> Peter
3. organising logistics (find a date, room booking, coordinating with admin, etc
    - Juju (?)
4. prerequisites (how to get a gitlab account, etc.)
    - Yingshi
6. checking that [git tutorials](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/gitlab-tutorials/) on community pages are up to date
    - Mohamed
7. High-level intro
    - Verena
9. preparing slides for github desktop
    - Peter & Lisa
10. preparing slides on gitlab browser interface
    - Miguel
11. creating (worked-through) examples and exercises
    - Miguel
12. testing exercises (and trouble-shooting)
13. presenting on the day
14. tutoring on the day
15. other ...

*Note*: Gaurav Bhalerao and Paul McCarthy have said they'd be happy to help out on the day as well.


### Course outline (DRAFT)
0. Course logistics
    - length: about 2h
    - room: ?
    - date: ?
    - integration with grad course ?
1. Prerequisites 
    - podcast/video introduction? 
        - [Github desktop tutorial](https://www.youtube.com/watch?v=JBW6-5_RhUU)
        - [Cass made a video already](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/)?
    - everyone should have git installed -- [install guide](https://www.atlassian.com/git/tutorials/install-git#:~:text=The%20easiest%20way%20to%20install,latest%20Git%20for%20Mac%20installer.)
    - everyone should have a gitlab account
        - Tell everyone to message open@win.ox.ac.uk if they are having trouble with this! Might sasve time in the actual tutorial 
    - ask everyone to send ther gitlab handle in advance
    - info on how to create an account: [here](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/2-1-starting-gitlab-account/) -- *is this up to date?*
    - all course material should be available online (and easily accessible)
2. Introduction (set up: 15 mins, intro: 10 mins)
    - presentation about basics, the usefulness of Github/Gitlab (version control, backup etc...) 
    - emphasise that git is very versatile and can be used in many different ways
    - one part about local git with app-interface: github desktop
    - one part about webinterface of gitlab
    - one part about git from command line (as little textboxes on slides that show relevant commands)
3. Practical (30-40 mins?)
    - main part
    - have a (separate) training repo for everyone to commit to
    - everyone following along through guided (by a presenter) examples -- switching between slides and live demonstration
    - tutors helping out with issues
4. Additional exercises
    - more examples and exercises for those who are quick to finish the first part


<br>

## Next Actions
- [ ] high-level intro slides
- [ ] Draft email with gitlab account set up instructions + how to set up Github desktop 
- [ ] Github desktop tutorial + slides for this part
- [ ] regular updates via slack and/or email
- [ ] next meeting: 18 July 2023

    

<br>

---

## Feedback on this session
*Please add a few words to each of the sections below so we can improve the experience of everyone.*
### What worked well?

### What surprised you?

### What would you change?

### What would you like to know more about?
