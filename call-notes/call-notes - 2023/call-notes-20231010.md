# Open WIN Ambassadors meeting - 10 Oct 2023

## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 10 Oct 2023, 10:00-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/Sk1pbXul6/edit)
- Last meeting notes: [26 Sep 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/BkPJOzlg6/edit)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Peter Doohan/ he/him / NDCN / @vyp730 / 0000-0003-2122-7354 
5. Juju Fars / they/them / NDCN / @pfr545 / 0000-0001-7771-5029
6. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789
7. Yingshi Feng / she/her / NDCN / @yingshif / 0000-0001-9065-4945
8. Verena Sarrazin/ she/her / Psychiatry / @verenasarrazin / 0000-0002-5796-5378

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World

#### A) WIN Monday messages
- [Peter's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/newsletter/open%20science%20bites.md)

#### B) Twitter
- [Juju's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/social_comm.md)
- twitter handle: [@OxfordWIN](https://twitter.com/OxfordWIN) ... ==please repost if on Twitter==
- Will post later this week, had to figure out with Hanna Smyth how to communicate in emails for specific tweets (threads) - Juju 

#### C) Poster repository 
- poll results generally very positve
- let's move forward with this: 
    - create a repo on the WIN community pages
    - populate with posters from our groups
    - write a short guide on how to use the repo

---

## Git/Gitlab training course

### ==Updates==

GitLab repository to collect all notes, material, presentation slides, etc.: [git course repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/GitLab%20training)


<*please add things below*>

1. [Peter] Finalise structure: write a proposed outline with timings

2. [Juju] organising logistics (find a date, room booking, coordinating with admin, etc

    - git course as part of a reboot camp: 
    - up to 3h in  (e.g. 1h presentation, 2h exercises?)
    - Jan 15-19 or April 22-26 (probably the better time, more coding heavy)
    - contact: Kamila Szulc-Lerch
    - course materials from Paul McCarthy on the Git course repository (if you use some of it, please agknowledge Paul) [Paul's Course](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/GitLab%20training/Git_course_PaulMcCarthy.pptx).

3. [Yingshi] prerequisites (how to get a gitlab account, etc.)
    - [google slides](https://docs.google.com/presentation/d/1Llg5J_1l2wwp6lbN9zztmvLX-RaHUvRPn9bUxUoHVt0/edit?usp=sharing)

4. [Mohamed] checking that [git tutorials](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/gitlab-tutorials/) on community pages are up to date

5. [Verena] High-level intro
    - [google slides](https://docs.google.com/presentation/d/1rLb2jRiYBTtYTN5pp79raAP4igegqOc8wnUQ32xrF1s/edit?usp=sharing)

6. [Peter, Lisa] preparing slides for github desktop

7. [~~Miguel~~, Verena] preparing slides on gitlab browser interface

8. [Verena, Juju] creating (worked-through) examples and exercises
    - idea: maybe combine with other python coding during boot camp?

9. testing exercises (and trouble-shooting)

10. other ...

### Creating a more detailed resources online
- building on existing pages
- adding more advanced information
- develop in parallel to git course
- iterate on slack to work collaboratively

### Timeline
- ==first draft: end of Oct?==

<br>

## Other updates

- Welcome to WIN session on 11 Oct: Bernd to give a 3min overview of the ambassadors programme

### Ambassadors workshop & dinner
- ==24th Jan 2024== (afternoon & evening)


### WIN Wednesday talk on data sharing:
- each about 10-15mins
- MEG on Xnat (*Duncan Smith?*)
- protocols db (*Verena, Yingshi*)
- Bruker integration (*Mohamed*)
- ==8th or 15th Nov== (12:00-13:00)

#### Additional WIN Wednesday talks
a) WIP on ambassadors programme -- probably best paired with a research project, not as a single session
b) open science success stories (*Lisa?*)
c) git presentation -- *Juju & Peter*

<br>

### Bugs & Issues
- ==!!== [list of open issues on gitlab](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues) ==!!==

<br>

## Next Actions
- [ ] continuing work on group and individual projects
- [ ] next group meeting: 17 Oct 

    

<br>

---

## Feedback on this session
*Please add any comments, suggestions, ideas, etc. below so that we can improve the experience of everyone.*

