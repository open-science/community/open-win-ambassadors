# Open WIN Ambassadors meeting - 12 Dec 2023

## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 12 Dec 2023, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/B1OXrYhrT/edit)
- Last meeting notes: [05 Dec 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rJ72ZkGST/edit)

-----

## Agenda
1. Quick intros
2. MR protocols db
3. Looking back on 2023
4. Plans and ideas for 2024
5. Admin

## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
3. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
5. Peter Doohan   / he/him   / NDCN       / @vyp730   / 0000-0003-2122-7354 
8. Mohamed Tachrount/ he/him / NDCN       / @mtach    / 0000-0003-1147-3412
11. Yingshi Feng  / she/her / NDCN        / @yingshif / 0000-0001-9065-4945
12. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----
## 1. Quick intros


<br>


## 2. MR protocols db
- outcomes from meeting on 07 Dec (Stuart, Dave Flitney, Duncan Smith, Juju, Mohamed, Verena, Bernd):
    - Check that open access approval system is working and ask existing protocol uploaders if they want to make them open (Dave)
    - ==Create poster for scan room (Open WIN Ambassadors)== and example email text for radiographers to send (Stuart)
    - Investigate xnat project for WIN wide sharing of example data (Duncan)
    - Investigate Bruker protocol import/parse (Dave/Mohamed)
    - Plan for publicity/recruitment (all – after January event)
    - Investigate google indexing and other bug fixes (Dave/Duncan)
- next steps:
    - see gitlab [issue 34](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/34)
    - ...


<br>

## 3. Looking back on 2023
- what have we done?
- what worked well?
- what could be improved?
- what should we continue doing?


**Previous/Outgoing Ambassadors: Would you like to stay involved?**


<br>

## 4. Plans and ideas for 2024
- outreach (WIN, NDCN, Uni, world)
- git course (grad programme, April 2024)
- community pages (updates, new content)
- poster repository (creation, engagement, maintenance)
- MR protocols database (engagement)
- WIN Wed seminars ?
- OS grad course ?
- FSL practicals ?
- sign up system for data sharing ?
- ...



<br>

## 5. Admin
- ==registration==: workshop and dinner: 24 Jan -- please [register here](https://app.onlinesurveys.jisc.ac.uk/s/oxford/registration-24-jan-2024-win-open-science-and-engagement-ambassadors-event) ❗️
- photos + blurb for webpage: Lilian?


<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*
