# Open WIN Ambassadors meeting - 19 Sep 2023

## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 19 Sep 2023, 10:00-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HJ4JiC8kp/edit)
- Last meeting notes: [12 Sep 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/S1UnboaA3/edit)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Peter Doohan/ he/him / NDCN / @vyp730 / 0000-0003-2122-7354 
5. Juju Fars / they/them / NDCN / @pfr545 / 0000-0001-7771-5029
6. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789
8. Verena Sarrazin/ she/her / Psychiatry / @verenasarrazin / 0000-0002-5796-5378

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World

#### A) WIN Monday messages
- [Peter's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/newsletter/open%20science%20bites.md)

#### B) Twitter
- [Juju's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/social_comm.md)
    - includes info on how to publish things on twitter
    - [first tweet](https://twitter.com/OxfordWIN/status/1701590777640739046) went out last week
    - twitter handle: [@OxfordWIN](https://twitter.com/OxfordWIN)
    - <mark> please repost if on Twitter </mark>

#### C) Poster repository 
- poll results generally very positve
- let's move forward with this: 
    - create a repo on the WIN community pages
    - populate with posters from our groups
    - write a short guide on how to use the repo

---

## Git/Gitlab training

### <mark>Updates</mark>

GitLab repository to collect all notes, material, presentation slides, etc.: [git course repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/GitLab%20training)


<*please add things below*>

1. [Peter] Finalise structure: write a proposed outline with timings

2. [Juju] organising logistics (find a date, room booking, coordinating with admin, etc

    - git course as part of a reboot camp: 
    - up to 3h in  (e.g. 1h presentation, 2h exercises?)
    - Jan 15-19 or April 22-26 (probably the better time, more coding heavy)
    - I am in contact with Kamila Szulc-Lerch and should have a meeting soon on how to organise the Git course inside the MRI graduate course. I will tell you more when the meeting date is chosen. 
    - I have gathered the course materials from Paul McCarthy and dropped it on the Git course repository (if you use some of it, please agknowledge Paul) [Paul's Course](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/GitLab%20training/Git_course_PaulMcCarthy.pptx).
    - **I will also do the "introduction to Open Science" during the first week of the MRI graduate course. This is only 15min long, let me know if you want to participate**. 

3. [Yingshi] prerequisites (how to get a gitlab account, etc.)

4. [Mohamed] checking that [git tutorials](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/gitlab-tutorials/) on community pages are up to date

5. [Verena] High-level intro

6. [Peter, Lisa] preparing slides for github desktop

7. [Miguel] preparing slides on gitlab browser interface

8. [Verena] creating (worked-through) examples and exercises

9. testing exercises (and trouble-shooting)

12. other ...

<br>

## WIN open science management meeting
- 18th Sep
- attended: Tom Nichols, Stuart Clare, Clare Mackay, Duncan Mortimer, Duncan Smith, Bernd
- brief update on ongoing projects
- Future of the ambassadors programme: advertisment for next cohort to go out in the next few weeks
    - thinking about some changes to the structure of the programme
    - keeping past ambassadors involved in some capacity?
- celebration dinner in January:
	- 1-2 external speakers -- any suggestions?
- data sharing (Duncan Smith):
	- Xnat (internal): automated MEG to Xnat relay is in place now
	- Xnat is integrated into calpendo: access managed through projects
	- Bruker integration is in the works (Mohamed)
	- sharing data externally: SDS or zenodo?
    - ideally need a small project (and lead volunteer) to go through the process -- any ideas?
- protocols db:
	- find out whether Bruker files can go in? -- Mohamed?
	- get radiographers to advocate (?)
	- how to raise awareness / user uptake? (e.g. at scan collection, at publication)
	    - send email to people who are currently scanning?
	- new [IT issue](https://git.fmrib.ox.ac.uk/acquisition/win-protocols-db/-/issues/49) to add robot control: i.e. get search engines to pick up on database entries
- 2-3 ambassadors to give a WIN Wednesday talk on data sharing:
	- MEG on Xnat
	    - *Mohamed?*
	- protocols db
	    - *Verena and Yingshi?*
	- Bruker integration
	    - *Mohamed?*
	- why, how, what ... practicalities and benefits of sharing data
	- goal: highlight what resources are available, demonstrate tools
- visibility:
    - organising (some) WIN Wednesdays: 
        - data sharing
        - WIP on ambassadors programme -- probably best paired with a research project, not as a single session
        - open science success stories (Lisa)
        - git presentation -- *Juju & Peter*
    - twitter, Monday messages, win-chat
	- meeting with management team
	- design a couple of flyers/posters to highlight e.g. protocols db -- to be put up around WIN

<br>

## Other project updates
- Oct 09th (intro week for grad course): Juju to give 15min general introduction to open science and what we are doing at WIN 
- Poster repository: Create the repository, and write the guidelines. 

### Bugs
- Open WIN website is getting out of date
    - e.g. Atom 
    - <mark> links to intranet need to be updated! </mark>

<br>

## Next Actions
- [ ] continuing work on group and individual projects
- [ ] full group meeting: 26 Sep 2023 or 03 Oct ?

    

<br>

---

## Feedback on this session
*Please add a few words to each of the sections below so we can improve the experience of everyone.*
### What worked well?

### What surprised you?

### What would you change?

### What would you like to know more about?
