# Open WIN Ambassadors meeting - 16 May 2023

## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 15 May 2023, 10:00-11:00 (BST)
- Join the call: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/H1xz43grh/edit)
- Last meeting notes: [27 April 2023](https://hackmd.io/@RbJyMIAdQPKtB6zew9DSUA/rkVL52PXn/edit)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Lisa Spiering / she/her / Exp Psychology / @wym003 / 0000-0002-9071-6541
2. Peter Doohan/ he/him / NDCN / @vyp730 / 0000-0003-2122-7354 
4. Mohamed Tachrount/ he/him / NDCN / @mtach / 0000-0003-1147-3412
5. Juju Fars / they/them / NDCN / @julienfars / 0000-0001-7771-5029
6. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World
- newsletter: https://www.win.ox.ac.uk/about/communications/newsletter 
- Monday messages: Peter to draft something 
- Twitter ?
    - just another channel? or is there a specific use case?
    - main WIN account not a good way to have 2-way communication with outside
    - create a separate Ambassadors account? -- Juju to lead
    - check with comms if they would agree with a separate account for the open science at WIN 
    - maybe ask previous ambassadors to help run the account
    - Juju : will email comms about that. Will send email to Jacqueline Pumphrey <jacqueline.pumphrey@ndcn.ox.ac.uk> , Hanna Smyth <hanna.smyth@ndcn.ox.ac.uk>and Carinne Piekema <carinne.piekema@ndcn.ox.ac.uk>

### Seminar series -- Success stories
- Lisa to check with Verena

### Gitlab training
- Mohamed to check with Miguel
- get the material of the course
    - follow the grad course 
    - prepare a practical 
### Protocols database and preclinical scanner
- The setup of the automatic upload of the preclinical data (Bruker MRI data) to XNAT is progressing well. Regular meetings with the IT guys are arranged to update each other.

### Electrophysiology database
- similar to BIDS but for mouse experiments
- Peter to create a summary 

### Behavioural tasks repository
- Within 3 weeks a version a the code will be available (with eye tracking)
- will be added to the MRI tasks repository ([tasks repository](https://git.fmrib.ox.ac.uk/open-science/tasks))

### Gitlab issues and community pages
- *everyone*: if there is anything out of date, please raise an issue on gitlab (or update community pages directly)

### open@win email
- Bernd to check with Cass if there have been any emails directly to her

<br>

## Next Actions
- [ ] [project template](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/projects/project_template.md)
- [ ] next meeting: 30 May 2023
    

<br>

---

## Feedback on this session
*Please add a few words to each of the sections below so we can improve the expereince of others.*
### What worked well?
-
### What surprised you?
- 
### What would you change?
- 
### What would you like to know more about?
-
