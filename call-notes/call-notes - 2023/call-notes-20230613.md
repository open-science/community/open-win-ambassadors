# Open WIN Ambassadors meeting - 13 June 2023

## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 13 June 2023, 10:00-11:00 (BST)
- Join the call: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rk44UcHwh)
- Last meeting notes: [06 June 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/B1sqgD2Ln/edit)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Peter Doohan/ he/him / NDCN / @vyp730 / 0000-0003-2122-7354 
2. Lisa Spiering / she/her / Exp Psychology / @wym003 / 0000-0002-9071-6541
3. Miguel Farinha/ he/him / @bpz864 / 0000-0002-1729-723X
5. Juju Fars / they/them / NDCN / @julienfars / 0000-0001-7771-5029
6. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World

#### A) WIN Monday messages
- [Peter's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/newsletter/open%20science%20bites.md)

#### B) Twitter
- [Juju's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/social_comm.md)
    - <mark>*everyone*: please add links, resources, ideas, content for threads</mark>
    - combination of polls (engagement) and information (links, resources)
- consider also Mastodon 
    - more single messages (rather than threads as on twitter)


### Seminar series -- Success stories
- 28th June: Verena giving a publication roundup talk
    - could add a few minutes for open science
    - Lisa to brainstorm with Verena about what to talk about
    - Miriam very happy to include this 
- speaker suggestion: maybe Amy Howard can talk about her experiences creating the BigMac dataset and making it publicly available (?)

### Git/Gitlab training
- Miguel's draft slides
- create a gitlab repo with all resources?

### Gitlab issues and community pages
- *everyone*: if there is anything out of date, please raise an issue on gitlab (or update community pages directly)

### Capturing open research at WIN
- <mark>*idea*: create a repo with posters from across WIN</mark>
    - create win-chat poll to ask how much interest there would be


<br>

## Next Actions
- [ ] [project templates](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/projects)
- [ ] continuing work on group and individual projects
- [ ] next meeting: 20 June 2023

    

<br>

---

## Feedback on this session
*Please add a few words to each of the sections below so we can improve the experience of everyone.*
### What worked well?

### What surprised you?

### What would you change?

### What would you like to know more about?
