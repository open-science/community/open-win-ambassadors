# Open WIN Ambassadors - 30th January 2023
## Agenda and collaborative notes

-----

**Important information**

- Date/time: Monday 30th January 2023, 10:00-11:30 (GMT/UTC)
- Join the call: MS Teams
- These notes: [https://hackmd.io/@FE_yEojCQPW4PD8RcTWR0A/BJDi-fSnj/edit](https://hackmd.io/@FE_yEojCQPW4PD8RcTWR0A/BJDi-fSnj/edit)
- Call recording: [Shared with Ambassadors only, requires SSO](https://unioxfordnexus-my.sharepoint.com/:v:/g/personal/psyc1182_ox_ac_uk/EWkTdmzqqbZJvuLRw3YjfAUBQKSqJUe4-NpiNzaiK-xSiQ?e=GVGp8a)



<mark>Yellow = action</mark>


-----

## Agenda
0. Participation guidelines and check-in
1. Celebration meeting - Feedback
2. Your project ideas
3. Feedback

## Participants
(name / Pronouns / Department / git handles (GitLab;GitHub) / ORCID)
1. Cassandra Gould van Praag / she/her / Psychiatry / @cassag;@cassgvp / 0000-0002-8584-4637
2. Lisa Spiering / she/her / Exp Psychology / @wym003;@lisaspiering / 0000-0002-9071-6541
3. Peter Doohan/ NDCN / @vyp730; @peterdoohan / 0000-0003-2122-7354
4. Miguel Farinha / @bpz864 ; @mlfarinha / 0000-0002-1729-723X
5. Juju Fars / they/them / NDCN / @julienfars / 0000-0001-7771-5029
6. Mohamed Tachrount/ he/him / NDCN / @mtach / 0000-0003-1147-3412

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.


-----

## 1. Celebration meeting - feedback
- Please send pictures of the mugs and dinner for the WIN newsletter!
- Meeting frequency
    - Feedback at the meeting that more opportunities to connect would be valuable.
    - Once a week then fortnightly.
    - Let's aim for a check in on slack in the between weeks. Prompt: "What have you been working on and does anyone need anything from someone."
    - More to fortnightly once everyone has a clear idea on what they are working on.

### Visibility
- URL redirect! [http://www.win.ox.ac.uk/open](http://www.win.ox.ac.uk/open)
- Twitter WIN open science success stories
    - Cass approaches people from the monday message, but it's low uptake
    - Could send from the WIN twitter and emphasis open science.
    - <mark>Peter: Prepare a draft tweet and outline which we can discuss with Jacqueline Pumphrey (NDCN Comms)</mark>
    - Work with Public Engagement team
- Ambassadors seminar series?
    - Not a lot of interest :)
    - Maybe ad hoc.
    - Mohammed would be willing to support
    - Lisa can help with the success stories

- Getting more use of Gitlab
    - sharing examples
    - supporting people with the first step
    - Maybe a focus group or survey to understand better how people want to use it.
        - intermediate version? everyone talk to their supervisors about GitLab and get their views on it to bring back to next meeting.
- Cass will write a report on the community day and publish on the community pages

## 2. Your project ideas
### Git
- Lots of work we could do to increase uptake
- have a conversation with PIs
- why use WIN GitLab vs github
    - safer (we own the infrastructure)
    - control
    - But what if PIs want to move?
- Something to unpick the tension/pain points to why people do/don't use git.
    - Survey
    - Focus group
- We have a spot in April graduate programme for git training.
- Include tasks
    - Juju has a battery of psychophys

### Success stories seminar
- Would like support in arranging etc.
- Feedback from the meeting is that people are hesitant or concerned about the labour of doing open science, not the that they're opposed to practice per se
- Show that it's not as complicated as they think. Normalising.


### Mouse data? + Digital Brain Bank
- Making it easier for cross species collaboration
- How to deal with massive datasets (PB!)
    - video, e-phys
- Lots coming from bigger institutes (e.g. Allen)
    - Could review what the larger centres do, think about the advantages and disadvantages
    - Sharing protocols
- Could talk with Benjamin Tendler to see what we can contribute to [digital brain bank](https://open.win.ox.ac.uk/DigitalBrainBank/#/)
- Research data management and community standards is not well understood
- Could roll into central WIN XNAT work and move to standardise data handling across the centre

### Analysis tools
- Make better links between OSL and Community Pages.
- Other group probably have cool software which we could support.


<mark>For next week: think about where you would like to put your energy and be prepared to roadmap.</mark>

## 3. Feedback on this session
<mark>Please add a few words to each of the sections below so we can improve the experience of others.</mark>
### What worked well?
-
### What surprised you?
-
### What would you change?
-
### What would you like to know more about?
-
