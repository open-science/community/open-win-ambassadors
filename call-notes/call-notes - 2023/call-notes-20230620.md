# Open WIN Ambassadors meeting - 20 June 2023

## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 20 June 2023, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HkJICjKwh)
- Last meeting notes: [13 June 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rk44UcHwh)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Miguel Farinha/ he/him / @bpz864 / 0000-0002-1729-723X
4. Mohamed Tachrount/ he/him / NDCN / @mtach / 0000-0003-1147-3412
6. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World

#### A) WIN Monday messages
- [Peter's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/newsletter/open%20science%20bites.md)


#### B) Twitter
- [Juju's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/social_comm.md)


### Seminar series -- Success stories
- catch-up with Lisa to ask whether there are any issues

### Git/Gitlab training
- maybe as a slack conversation: <mark>get structure and logistics down</mark>
    - how much time do we need?
    - will there be separate sessions? (beginners, intermediate)
    - how to split (or combine) git in the browser with git from command line?
    - what are the prerequisites for attendants? (gitlab account, anything else?)
    - who will deliver the presentation(s)?
    - who will be there as tutor?
    - Gaurav Bhalerao has given git tutorials before and would be interested to help as tutor
    - can we ask Paul McCarthy to help? (e.g. as online tutor)

- once fully fleshed out: get in touch with Kamila, Tom Okell, etc. to check how/if this can be integrated into the grad course


### Preclinical scanner
- MEG is ready on XNAT
- preclinical scanner almost ready
    - needs some discussions with PI
- other MRI systems will follow
- all internal for now

### Gitlab issues and community pages
- <mark>*everyone*: some sections are getting out of date (!) ... please raise an issue on gitlab (or update community pages directly)</mark>

### NDCN away-day
- 22 June at the National History museum
- Bernd to give a (very) short presentation of the Ambassadors programme

### analysis pipeline
- any plans to work on this?





<br>

## Next Actions
- [ ] [project templates](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/projects)
- [ ] continuing work on group and individual projects
- [ ] next meeting: 27 June 2023

    

<br>

---

## Feedback on this session
*Please add a few words to each of the sections below so we can improve the experience of everyone.*
### What worked well?

### What surprised you?

### What would you change?

### What would you like to know more about?
