# Open WIN Ambassadors meeting - 05 Dec 2023

## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 05 Dec 2023, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rJ72ZkGST/edit)
- Last meeting notes: [28 Nov 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HyMe6DsBp/edit)

-----

## Agenda
1. Using markdown and hackmd
2. Community Pages
3. Gitlab
4. Admin & logistics
5. Actions

## Participants
(name             / pronouns / department / GitLab ID   / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329     / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
5. Bernd Taschler / he/him   / BDI        / @ndcn1032   / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----
## 1. Using markdown and hackmd
- headers
- text: *italic*, **bold**, ~~strikethrough~~, text~subscript~, text^superscript^, ==highlight==, `code`
- links
- bullet list
- numbered list
- checkbox
- horizontal line
- more space
- quote
- code chunks
- insert images 
- comments

**References**
- markdown [tutorial](https://www.markdowntutorial.com/)
- another markdown [tutorial](https://commonmark.org/help/tutorial/)
- overview of markdown [syntax](https://www.markdownguide.org/basic-syntax/)
- our [info on the community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/1-2-tools-markdown/) could be improved

----
## TEST AREA
### 3rd
#### 4th level

normal text ... *italic text* ... **bold** ... ***bold italic***  ~~something wrong~~ ... ==something important==

`code` ... `X, Y`

[text](https://www.markdowntutorial.com/)

- first item
- second item

1. first
2. second
3. third

- [ ] TODO item

---

<br>

> quote - something someone said

```
x = a+b
$ cat hello world
```

![text](path-to-image)

<!--

anything written is a comment
;lasdkjpow-

-->


----

<br>

## 2. Community Pages
- short redirect: www.win.ox.ac.uk/open
    - (instead of full link: https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/ )
- have a look around!


<br>

## 3. Gitlab 
- info and tutorials on the community pages about [git and gitlab](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/)
- [info on intranet](https://www.win.ox.ac.uk/research/it/i-want-to/programming/git)
- new project members
    - ==ask IT to create an account for Lara and Lilian==
- project structure: [highest level](https://git.fmrib.ox.ac.uk/open-science)
- [ambassadors repository](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors)
    - our "playground"
    - contents
    - editing a file in the browser
    - issues
    - create / edit / comment on a new issue
    - editing a file
- [community pages repository](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community)
    - a) directly in the browser: commit to a new/separate branch (not `main`)
    - b) pushing a local branch
    - best practice for any edits: merge requests

### Text editors
- e.g. [Sublime text](https://www.sublimetext.com/)
- see info on community pages [here](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/1-3-tools-atom/)

<br>

## 4. Admin
- meeting dates in Jan
- photos and blurb for community pages [here](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/ambassadors/meet_ambassadors/) ... e.g. 1-2 sentences about what motivates you to be an open science ambassador
- open@win.ox.ac.uk email
- workshop and dinner: 24 Jan


<br>

## 5. Next Actions
- [x] next meeting: 12 Dec 2023, 10am (with previous ambassadors)
- [x] first meeting in 2024: 09 Jan, 10am ?



<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*
