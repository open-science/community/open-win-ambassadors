# Open WIN Ambassadors meeting - 30 May 2023

## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 30 May 2023, 10:00-11:00 (BST)
- Join the call: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/BkInD7783/edit)
- Last meeting notes: [16 May 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/H1xz43grh/edit)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Peter Doohan/ he/him / NDCN / @vyp730 / 0000-0003-2122-7354 
3. Miguel Farinha/ he/him / @bpz864 / 0000-0002-1729-723X
5. Juju Fars / they/them / NDCN / @julienfars / 0000-0001-7771-5029
6. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World
- newsletter: https://www.win.ox.ac.uk/about/communications/newsletter 
- Monday messages: Peter to coordinate with Andrew 
- Twitter account
    - Bernd to follow up with Stuart

### Seminar series -- Success stories
- no updates

### Gitlab training
- Miguel to draft a content outline
- get the material of the course
    - follow the grad course 
    - prepare a practical 

### Protocols database and preclinical scanner
- no updates

### Electrophysiology database
- similar to BIDS but for mouse experiments
- Peter to create a summary 

### Behavioural tasks repository
- Within 3 weeks a version of the code will be available (with eye tracking)
- will be added to the MRI tasks repository ([tasks repository](https://git.fmrib.ox.ac.uk/open-science/tasks))

### Gitlab issues and community pages
- *everyone*: if there is anything out of date, please raise an issue on gitlab (or update community pages directly)

### WIP issues
- PE team not able to modify Cass's repository
- can we copy the repo (currently hosted on Cass' private account) to the open WIN repo?
- Juju in touch with Jacquline, Carinne
    - Bernd to follow up with Stuart et al. 

### open@win email
- query about fsl installation -- https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Support 

### Future of Open WIN ambassador programme
- Bernd is meeting with Stuart, Heidi, Tom Nichols on 30 May


<br>

## Next Actions
- [ ] [project template](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/projects/project_template.md)
- [ ] continuing work on group and individual projects
- [ ] next meeting: 13 June 2023
    

<br>

---

## Feedback on this session
*Please add a few words to each of the sections below so we can improve the expereince of others.*
### What worked well?
-
### What surprised you?
- 
### What would you change?
- 
### What would you like to know more about?
-
