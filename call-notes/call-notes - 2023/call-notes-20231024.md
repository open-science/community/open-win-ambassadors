# Open WIN Ambassadors meeting - 24 Oct 2023

## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 24 Oct 2023, 10:00-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/ryEVChoWT/edit)
- Last meeting notes: [17 Oct 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rJ5RRVVWT/edit)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Juju Fars / they/them / NDCN / @pfr545 / 0000-0001-7771-5029
6. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789
7. Yingshi Feng / she/her / NDCN / @yingshif / 0000-0001-9065-4945
8. Verena Sarrazin/ she/her / Psychiatry / @verenasarrazin / 0000-0002-5796-5378

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World

#### A) WIN Monday messages
- [Peter's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/newsletter/open%20science%20bites.md)

#### B) Twitter
- [Juju's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/social_comm.md)
- twitter handle: [@OxfordWIN](https://twitter.com/OxfordWIN) ... please repost if on Twitter

#### C) Poster repository 
- poll results generally very positve
- let's move forward with this: 
    - create a repo on the WIN community pages
    - populate with posters from our groups
    - write a short guide on how to use the repo

#### D) Flyers
- see [issue #28](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/28)

---

## Git/Gitlab training course

### ==Updates==

GitLab repository to collect all notes, material, presentation slides, etc.: 
- [git course repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/GitLab%20training)
- see [issue #18](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/18)


1. [Peter] Finalise structure: write a proposed outline with timings

2. [Juju] organising logistics (find a date, room booking, coordinating with admin, etc

    - git course as part of a reboot camp: 
    - up to 3h in  (e.g. 1h presentation, 2h exercises?)
    - Jan 15-19 or April 22-26 (probably the better time, more coding heavy)
    - contact: Kamila Szulc-Lerch
    - course materials from Paul McCarthy on the Git course repository (if you use some of it, please agknowledge Paul) [Paul's Course](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/GitLab%20training/Git_course_PaulMcCarthy.pptx).

3. [Yingshi] prerequisites (how to get a gitlab account, etc.)
    - [google slides](https://docs.google.com/presentation/d/1Llg5J_1l2wwp6lbN9zztmvLX-RaHUvRPn9bUxUoHVt0/edit?usp=sharing)

4. [Mohamed] checking that [git tutorials](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/gitlab-tutorials/) on community pages are up to date

5. [Verena] High-level intro
    - [google slides](https://docs.google.com/presentation/d/1rLb2jRiYBTtYTN5pp79raAP4igegqOc8wnUQ32xrF1s/edit?usp=sharing)

6. [Peter, Lisa] preparing slides for github desktop

7. [Verena] preparing slides on gitlab browser interface

8. [Verena, Juju] creating (worked-through) examples and exercises
    - idea: maybe combine with other python coding during boot camp?

9. testing exercises (and trouble-shooting)

10. other ...

### Creating a more detailed resources online
- building on existing pages
- adding more advanced information
- develop in parallel to git course
- iterate on slack to work collaboratively

### Timeline
- ==first draft: ??==
- Oxford-Berlin autumn school: 21 Nov 2023
- training for new ambassadors: Dec 2023

<br>

## Other updates

- applications for 2024 cohort are now open: [here](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/ambassadors/application/)
- asked Stuart to put up a slide on the common room screen

### protocols database
- is it possible to add non-human protocols as well?
    - e.g. issues with different file formats
    - 

### Ambassadors workshop & dinner
- 24th Jan 2024 (afternoon & evening)


### WIN Wednesday talk on data sharing:
- each about 10-15mins
- MEG on Xnat (*Duncan Smith? -- at some later date*)
- protocols db (*Verena, Yingshi*)
- Bruker integration (*Mohamed*)
- short overview of open science at WIN and the ambassadors programme (*Bernd*)
- ==8th Nov== (12:00-13:00)

#### Additional WIN Wednesday talks
a) open science success stories (*Lisa?*)
b) git presentation -- *Juju & Peter*

<br>

### Bugs & Issues
- ==!!== [list of open issues on gitlab](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues) ==!!==
- e.g. [issue #26](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/26) -- review process in protocols db

<br>

## Next Actions
- [ ] continuing work on group and individual projects
- [ ] next group meeting: 31 Oct 

<br>

---

## Feedback on this session
*Please add any comments, suggestions, ideas, etc. below so that we can improve the experience of everyone.*
