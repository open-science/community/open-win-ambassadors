## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 27 June 2023, 10:00-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/H15EPzdO3)
- Last meeting notes: [20 June 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HkJICjKwh)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Peter Doohan/ he/him / NDCN / @vyp730 / 0000-0003-2122-7354 
2. Lisa Spiering / she/her / Exp Psychology / @wym003 / 0000-0002-9071-6541

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## Project Updates

### Outreach -- WIN to WIN / WIN to World

#### A) WIN Monday messages
- [Peter's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/newsletter/open%20science%20bites.md)
- <mark>we should aim to keep this going (ideally a message every week)</mark>

#### B) Twitter
- [Juju's drafts](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/comms/social_comm.md)



### Seminar series -- Success stories
- <mark>28 June: Verena Sarrazin</mark>

### Git/Gitlab training
- <mark>[issues from last meeting](https://hackmd.io/oY6wtTtkRz6l4qYWKR2-Wg#GitGitlab-training)</mark>

### Preclinical scanner


### Electrophysiology database


### Behavioural tasks repository


### Gitlab issues and community pages
- <mark>*everyone*: if there is anything out of date, please raise an issue on gitlab (or update community pages directly)</mark>

### Capturing open research at WIN


### Recap: Thomas Willis day / NDCN away-day
- 22 June at the Natural History museum
- Bernd gave a short presentation about the Ambassadors programme
- [Anna Guttesen](https://www.ndcn.ox.ac.uk/team/anna-guttesen) gave a short talk on preregistration
    - she is very keen on open science issues and might be able to help on projects if/when needed
- Bernd had a chat with Paul McCarthy: he'd be very happy to help out with the git sessions



<br>

## Next Actions
- [ ] [project templates](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/projects)
- [ ] continuing work on group and individual projects
- [ ] next meeting: 04 July 2023

    

<br>

---

## Feedback on this session
*Please add a few words to each of the sections below so we can improve the experience of everyone.*
### What worked well?

### What surprised you?

### What would you change?

### What would you like to know more about?
