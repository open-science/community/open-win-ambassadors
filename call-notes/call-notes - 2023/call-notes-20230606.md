# Open WIN Ambassadors meeting - 06 June 2023

## Agenda and collaborative notes

-----

**General information**

- Date/time: Tuesday 06 June 2023, 10:00-11:00 (BST)
- Join the call: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/B1sqgD2Ln/edit)
- Last meeting notes: [30 May 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/BkInD7783/edit)

<mark>Yellow = action</mark>

-----

## Agenda
0. Participation guidelines and check-in
1. Group projects
2. Individual projects
3. Feedback

## Participants
(name / Pronouns / Department / GitLab / ORCID)
1. Lisa Spiering / she/her / Exp Psychology / @wym003 / 0000-0002-9071-6541
3. Miguel Farinha/ he/him / @bpz864 / 0000-0002-1729-723X
5. Juju Fars / they/them / NDCN / @julienfars / 0000-0001-7771-5029
6. Bernd Taschler / he/him / BDI / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

### Notes from Open WIN management meeting (30 May 2023)
- Briefly discussed all ongoing group and individual projects. — They were generally very supportive of everything you are doing / planning to do!
    - @Juju - behavioural tasks project: Clare and Heidi mentioned that Laurence Hunt has collected some bits and pieces some time ago (they were not sure whether this is somewhere on gitlab or not) … it might be worth checking with Laurence directly in case there is anything useful you could use or adapt. 
- Future of the ambassadors programme:
    - Strong agreement from everyone that we want to continue the ambassadors programme, including a new cohort for next year. 
    - Cass’ position is not being filled in the short term (essentially because the WIN grant only runs until 2024 and it wouldn’t make sense to open a call for a position that only lasts a few months)
    - Will probably get admin help from others (e.g. Nicky from NDCN admin)
- **Git/Gitlab tutorial** (or even full open science course):
    - Everyone is fully on board with the git training 
    - Integration into the grad course: need to talk to Kamila 
    - Clare suggested to even create a full “open science” module for the grad course that could include a range of topics (e.g. git, data sharing, research culture, etc.) … *Let’s think about this! It would be a big accomplishment, but probably requires a bit more of everyone’s time ...*
- **Twitter:**
    - Stuart and Heidi are hesitant to set up a separate open science account (mainly because they prefer a single official WIN channel and want to showcase open science initiatives directly there. For example, they had the same discussion with EDI group a while ago and decided to only use the main account.)
    - Stuart emphasised that there shouldn’t be any barriers to get messages out on the main account.
    - Maybe set up a meeting with Stuart and Jacqueline 
    - Their proposal was a "trial month” of using the main account to see whether there are any barriers / impracticalities and what kind of interactions we could have with the general public. 
    - Could include a specific icon / banner / header in tweets to indicate open science related content. 
    - If this doesn’t work satisfactorily for us, they remain open to a separate account.
- Data sharing issues:
    - XNAT (internal), zenodo (external), etc. 
    - Difficult legal issues around ethics / contracts to share data externally
    - Dementia platform UK as a potential model framework for external data sharing
- Ambassadors as essential in **capturing open research** across the whole of WIN
    - Heidi sees the ambassadors as being crucial to spot, record, collect open science (in the broad sense) related research outputs across WIN — including data sharing examples, activities with research groups, talks, lab culture best practices, etc. 
    - Twitter could be ideal for showcasing this.
- Agreement to organise a celebratory meeting again (similar to the one Cass organised in January with old & new ambassadors plus externals)
    - Especially Stuart viewed that meeting as a big success and is keen to repeat it (probably around January again)
    - Tom and Stuart agreed to help organise this.

---

## Project Updates

### Outreach -- WIN to WIN / WIN to World
- Monday messages 
- Twitter:
    - <mark> *everyone*: think about potential tweets, aiming for a list of 5-10 to get started </mark>
- Capturing open research activities at WIN
    - <mark> *everyone*: spot ongoing open science activities (sharing data, code, tutorials, papers, etc.) across as many WIN research groups as possible</mark>

### Seminar series -- Success stories


### Gitlab training
- Miguel to draft a content outline
- get the material of the course
1. Create your own repository.
2. Upload a file and modify it.
3. Check status of repository with git status.
4. Do a git add, git commit and git push.
5. We create a repository and make students collaborators.
6. Students clone/fork the repository, git clone.
7. Students create a branch, git branch.
8. Students create a pull request.
9. Talk about creating issues.
10. Speak about LICENSE.md, CONTRIBUTING.md, ACKNOWLEDGEMETS.md, DOI. Make repository citable, create a DOI in Zenodo.

- <mark>Next week: Miguel start preparing some slides using the web browser.</mark>


<br>

## Next Actions
- [ ] [project templates](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/projects/project_template.md)
- [ ] continuing work on group and individual projects
- [ ] next meeting: 13 June 2023
    

<br>

---

## Feedback on this session
*Please add a few words to each of the sections below so we can improve the experience of everyone.*
### What worked well?

### What surprised you?

### What would you change?

### What would you like to know more about?
