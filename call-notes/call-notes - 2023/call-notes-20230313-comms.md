# Open WIN Ambassadors - 13th March 2023
## Agenda and collaborative notes

-----

**Important information**

- Date/time: Monday 13th March 2023, 10:00-11:30 (GMT/UTC)
- Join the call: MS Teams
- These notes: [live notes on hackmd](https://hackmd.io/@FE_yEojCQPW4PD8RcTWR0A/r1hswd3kh)
- Call recording: [Shared with Ambassadors only, requires SSO - Link to be updated](https://unioxfordnexus-my.sharepoint.com/:v:/g/personal/psyc1182_ox_ac_uk/Ea8uJH68E8JKtfONxvj7tmYBgEfSnFnV5p6WInRVqpr6iA?e=Rh2I8F)



<mark>Yellow = action</mark>


-----

## Agenda
0. Participation guidelines and check-in
1.

## Participants
(name / Pronouns / Department / git handles (GitLab;GitHub) / ORCID)
1. Cassandra Gould van Praag / she/her / Psychiatry / @cassag;@cassgvp / 0000-0002-8584-4637
2. Lisa Spiering / she/her / Exp Psychology / @wym003;@lisaspiering / 0000-0002-9071-6541
3. Peter Doohan/ NDCN / @vyp730; @peterdoohan / 0000-0003-2122-7354
4. Miguel Farinha / @bpz864 ; @mlfarinha / 0000-0002-1729-723X
5. Juju Fars / they/them / NDCN / @julienfars / 0000-0001-7771-5029
6. Mohamed Tachrount/ he/him / NDCN / @mtach / 0000-0003-1147-3412

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.


-----

## 1. Setting up HackMD for OPENWIN
- <mark>Peter to try and set this up ready for next week</mark>
    - See [this issue on ambassadors gitlab](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/8)

## 2. Preparing for Meeting with PE and comms
- [Our first thoughts on open science comms padlet](https://padlet.com/WINPublicEngagement/open-win-ambassadors-2023-projects-monday-message-c83nu0jzncbmvhyv)
- TODO: see a few sample posts
    - see [this on the ambassadors gitlab](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/newsletter/open%20science%20bites.md)
    - change title from OPEN SCIENCE to something specific
        - eg, WIN data management plans ...
    - <mark>Cass: Update the titles for the Monday-message-type </mark>
    - Distinguish between comms message types
        - WIN2WIN
            - <mark>Cass: move bites to new subheading</mark>
        - WIN2WORLD
            - <mark>Peter to add tweet thing</mark>
- TODO: get a better understanding of who the targeted audience(s) will be for these news items
    - WIN2WIN: audience WIN
        - specific groups within WIN?
            - students
            - Open Science vets/noobs?
            - PIs
            - ECRs
            - <mark>Cass: Under each WIN2WIN message idea, add the specific target audience within WIN, first thing under the title </mark> (e.g. example 3 could target PIs, 4 more for students/ECRs)
    - WIN2WORLD: people that follow WIN on Twitter etc (Public facing but also lots of WIN people follow).
        - <mark> ASK comms: do we know the demos of the WIN social media accounts?
        - audience is more public facing than WIN2WIN
- TODO: what the goals are (e.g. more people using the resources you mentioned?).
    - split goals by target audience
    - WIN2WIN: connect WINners to resources
    - increase OPEN SCIENCE proficiency at WIN
    - highlight successes at WIN
- TODO: example(s) of a prior post(s) on Open WIN that has done well (however you’re choosing to measure that) and you’re hoping to emulate.
    - see success stories below



## 3. Success Stories Seminars
Speaker ideas
- Tulika Nandi <tulika.nandi@ndcn.ox.ac.uk> (ex postdoc with Charlie Stagg, moving/moved to Mainz) - I know she’s publishing work with some shared data and maybe code?
- Charlotte Rae <C.Rae@sussex.ac.uk>  (ex Oxford undergrad, now PI at Sussex) - publishing all her code and anonymous data)
- Janine Bijsterbosch <janine.bijsterbosch@wustl.edu> (ex Oxford postdoc, now PI at Washington) - my guess is she’s running her whole lab as open as possible. She was in the Analysis group, so she will draw a good/influential crowd.
- Verena Sarrazin (verena.sarrazin@psych.ox.ac.uk) - someone contacted her about using her data
- We could get in touch with people involved in the Open WIN resources' projects like UK Biobank, Human Connectome Project (HCP), Developing Human Connectome Project (dHCP), ... https://www.win.ox.ac.uk/open-win/open-win-resources
- <mark>TODO: set the ‘brief’ for our speakers. Do we want them to talk about their [published] work, or their open science practices, or both? Would be great to keep these to short presentations (e.g. 20 min + 10 min questions) to keep it light.</mark>
- Hesitant on time commitment / workload
- 15-20 min talk + questions

Where will we host the speakers
- WIN Wednesday
- book on Calpendo to get a room and a time

How to coordinate with speaker
- just email them and give them options for rooms and times

Recording
- Andrew does all the recording
- You just need to turn up if you can, speaker might be remote. Talks will always be hybrid

What do we actually have to do?
- organise a time with the speaker
- chair the chat

<mark>Lisa to coordinate with Verena to set up the first Seminar!</mark>


# Notes from meeting with Jacquline Pumphry (Comms) and Hanna Smyth (PE)
## Juju's notes

- People are on board (Jacqueline and Hanna).
- We would use the same twitter account as the PE team (global win twitter account)
- Discussion about target audiences
- using comms and not PE because our audience is mainly academics

## Cass's notes

- Use the WIN twitter account - submit to comms@win.ox.ac.uk
- Like the mix of resources and people highlighting (good for traction)
- Who is following twitter?
  - Hard to find out
  - JP: About to do a wider review of twitter accounts in Ox Neuroscience (chase JP for that later)
- What is our aim for communicating to the public?
  - What to we want them to do/feel/respond?
  - **Let them know about resources available for re-use**
    - Inc good examples of re-use of others work
  - Use our platform to promote open science
  - Mutual promotion across Oxford social media account
- HS: update comms guidance to include suggestion to add links to data/code
  - **<mark>We draft a sentence or two guidance and send to JP</mark>**

- Newsletter: Have testimonials form people using infrastructure in the newsletter. Andrew can track click through

- Post Monday-message items to WIN chat when appropriate

- The difference between comms and PE - learning from the public in mutually beneficial engagement, cf comms which is more one way.
  - Do polls, ask questions.  
