:::info
- Date/time: Tuesday 04 June 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/B104kjlmA/edit)
- Last meeting notes: [14 May 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HkKSWijGA/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 04 June 2024

## Agenda
1. Admin
2. Licensing and copyright
3. Other things?


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
3. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Admin

#### Recap: WIN OS Management meeting (16 May)
- updates on recent activities
- DPA/DPIA workshop recap (still a lot confusion)
    - data sharing effort (Marieke Martens, Lilian?)
    - anyone else creating a DPIA at the moment?
- git/gitlab training (WIN IT): 
    - old recordings on canvas
    - IT guys are happy to collaborate on future git training (incl. branches, CI tools, gitlab pages, etc.)


#### Prep for hack-day (11 June)
- FMRIB Annexe, Cowey 2 room
- start: 9am (roughly)
- lunch provided (sandwiches)
- Stuart will join us 1-2pm
    - any questions, issues, ideas, etc. 
- end: 3pm (roughly)
- video recordings:
    - camera
    - statements
    - *hair cut*?
- community pages hacking:
    - gitlab / markdown
    - skim through community pages beforehand
    - laptop (plus charger)



## 2. Licensing and copyright

#### Resources
- [community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/repo-license/)
- [Turing Way guide](https://the-turing-way.netlify.app/reproducible-research/licensing.html)
- [online tool to check funder's requirements](https://v2.sherpa.ac.uk/juliet/about.html)


#### Types of licenses
![Creative Commons licenses](https://the-turing-way.netlify.app/_images/cc-license-chart.png)




#### FAQ's
1. What is the difference between license and copyright?
"*Copyright is the legal term used to declare and prove who owns the intellectual property (the code, text, etc.). Licensing is used to describe the terms under which people are allowed to use the copyrighted material.*"

2. What license should I use?
- What does the university encourage to use?
- differences between software, code and data
- case by case basis
- most common: CC-BY
- default: CC-BY (?)
    - with additional points about what you could be worried about
- if you care about commercial use: CC BY-NC
- you can always make it more permissive later on
- ND versions seem to go somewhat against the idea of open science

3. What is an Apache license?
"The Apache License is permissive; unlike copyleft licenses, it does not require a derivative work of the software, or modifications to the original, to be distributed using the same license. It still requires application of the same license to all unmodified parts. In every licensed file, original copyright, patent, trademark, and attribution notices must be preserved (excluding notices that do not pertain to any part of the derivative works). In every licensed file changed, a notification must be added stating that changes have been made to that file."
- use version control to track changes / modifications

4. What are share-alike licenses?

5. What are copyleft licenses?
- [wikipedia](https://en.wikipedia.org/wiki/Copyleft)

6. What is meant by commercial use?
- e.g. Journalists, 
- is the CC BY-NC license technically not full open science?
- aren't journals commercial as well?

7. Where should I put a license statement
- readme or citation file

8. Is licensing relevant for DPA/DPIA?

9. Should we think about patents?
- not really in the spirit of open science
- refer to external resources (e.g. innovation office)
- possibly, can still share your code while submitting a patent application? (check)



...



## 3. Any other things ?


---

## ==Actions==
❗️ see gitlab issues: [ambassadors repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues) and [community repo](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues)


<br>

## Upcoming
- [ ] Next meeting: 11 June, 9am-3pm
- [ ] 18 June: Thomas Willis Day (NDCN away-day)
- [ ] 09 July: discussion with Iske Bakker and Kaitlin Krebs on funders' requirements, OA publishing, etc.
- [ ] Aug: WIN Wed on preregistration

<br>

---

## Feedback
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

