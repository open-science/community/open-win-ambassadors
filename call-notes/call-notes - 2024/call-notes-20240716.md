:::info
- Date/time: Tuesday 16 July 2024, 14:00-15:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/Syacbdsq0/edit)
- Last meeting notes: [09 July 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HJvzQdqPC/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 16 July 2024

## Agenda
1. Discussion about OA policies and requirements


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Iske Bakker    / she/her  / FMRIB      /    / 
2. Kaitlin Krebs  / she/her  / FMRIB      /    /
3. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
4. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
5. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
8. Juju Fars      / they/them / NDCN      / @pfr545   / 0000-0001-7771-5029
9. Yingshi Feng   / she/her / NDCN        / @yingshif / 0000-0001-9065-4945
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789


## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Discussion: OA policies, requirements
- see university's recently updated OA policy
- acknowledging Wellcome:
	- any scanning, cluster use, WIN resources are based on Wellcome funding
	- essentially, always include WIN centre grant plus any PI grants
	- keep acknowledging in coming years (even after Wellcome grant has finished) for any work done during the grant period
	- Wellcome will flag any papers not in PMC record
- BRC requirements similar to Wellcome
- how to deal with acknowledgements in external collaborations:
	- email every co-author early about requirements
	- always include a rights retention text (applies a CC-BY licence) in author-approved manuscript
	- include rights retention text in acknowledgment section and in cover letter
	- self-archive any accepted manuscript
	- see [template on WIN website](https://www.win.ox.ac.uk/research/publications/win-affiliations-acknowledgements)
- Wellcome's data sharing requirements
	- as much as possible but in accordance with ethics (for each study)
- paper fees:
	- payed usually by the corresponding author's institution
	- Uni Ox has a block grant from Wellcome
	- check journal before writing up
	- reviews, technical notes *not* covered by block grant (!)
- green OA is university's preferred option
	- self-archiving solves every OA issue!
- licence:
	- papers: only CC-BY (without extension, otherwise need pre-approval from Wellcome)
	- software / data: ??



### Resources
- [WIN's OA policy](https://www.win.ox.ac.uk/research/publications/open-access-policy)
- [Wellcome's OA guidelines](https://wellcomeopenresearch.org/for-authors/data-guidelines)
- [Research Data Oxford](https://researchdata.ox.ac.uk/)
- Bodlean pages for each funder


<br>

---

## Feedback
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*