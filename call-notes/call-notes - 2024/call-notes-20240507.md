:::info
- Date/time: Tuesday 07 May 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HkYBb_5e0/edit)
- Last meeting notes: [16 Apr 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/ry1DYLPfC/edit)
- highlighted: ==important action items==
:::

# Open WIN Ambassadors meeting - 07 May 2024

## Agenda
1. Recap reboot camp module


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Recap reboot camp

### Feedback
- generally everything went well
- good timing
- about 10 students
- not very responsive / interactive (esp. 1st part)
- *idea*: create a handout with links (?)
- all slides shared on the git repo and with Kamilla
- git: right level for this audience, working with repo was fine
- might make sense to have the module earlier in the week (i.e. covering the whole research process early)
- prerequisites: maybe give a primer before

## 2. Other things
- [ ] Integrating Xenia's EBRAINS guide into community pages
- [ ] WIN Wed: preregistration - 26 June (Anna, Lisa, Verena, Jackie Thompson)
    - anyone who could show neuroimaging example of preregistered study??
- [ ] poster repository!
- [ ] Anna is joining the E-REPRO-NIM (?)


---

## ==Actions==
- [ ] see gitlab issues
- [ ] collecting posters from various groups
- [ ] send recorded ambassador statements to Anna!
- [x] Bernd to send out email with topics to cover next

<br>

## Upcoming
- [ ] Next meeting: 14 May, 10am 
- [ ] Thomas Willis Day (NDCN away day): 18 June


<br>

---

## Feedback
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

