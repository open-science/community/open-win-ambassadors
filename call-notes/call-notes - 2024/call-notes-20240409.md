:::info
- Date/time: Tuesday 09 Apr 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/H1MTwQ-lA/edit)
- Last meeting notes: [02 Apr 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/S12taPd10/edit)
- highlighted: ==important action items==
:::

# Open WIN Ambassadors meeting - 09 Apr 2024

## Agenda
1. Preparing reboot camp module


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
8. Juju Fars      / they/them / NDCN      / @pfr545   / 0000-0001-7771-5029
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----


## 0. Logistics
**Grad course / Reboot camp**
- Thu, 25 April, 10:00-13:00
- FMRIB annexe, Cowey room
- organised by: Kamila Szulc-Lerch, Tom Okell
- *Note: Bernd is away 18 April - 06 May*


## 1. Resources and materials
- old presentations:
    - [existing OS presentations](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/presentations?ref_type=heads)
    - [existing git presentations](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/GitLab%20training?ref_type=heads)
    - [additional material -- gitlab issue](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/18)
- new ==[gitlab repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/Open-science-training)== for reboot camp


## 2. Open science part
*Main Idea*: going through the full life cycle of a project (incl. individual steps, data, code, ethics, licence, publishing, paper, etc.)
1. Lara: overview, recap of OS issues - (15min)
2. Juju: (fake) research idea, ethics - (10min)
3. Anna: preregistration, OSF, analysis ideas, code for pilot data - (15min)
4. Lilian, Juju: MR protocol - (5min)
5. Lilian: data sharing, code sharing (with / without comments), task sharing, readme - (15min)
6. Juju: licensing and copyrights - (5-10min)
7. Lilian, Lara: preprints, open access publishing - (10min)

for dummy data: have a few lines with identifyers - task is to remove identifyer before pushing online

==**Open Tasks**==
- [x] estimate how long each part will take
- [x] prepare slides
- [ ] put everything together into one presentation/slidedeck - *Lara*
- [x] practice run: 09 April


## 3. Git part
**Next Steps**
- [ ] prepare prerequisites to send to Kamila
- [ ] practice run: ==16 April==


## 4. Other things
- [x] Thanks to Lara for keeping the WIN Monday messages rolling!
- [ ] WIN Wed: preregistration - 26 June (Anna, Lisa, Verena, Jackie Thompson)
    - anyone who could show neuroimaging example of preregistered study??
- [ ] poster repository!



---

## ==Actions==
- [ ] ❗️preparing material for reboot camp❗️
- [ ] see gitlab issues
- [ ] collecting posters from various groups
- [ ] send recorded ambassador statements to Anna!

<br>

## Upcoming
- [ ] Next meeting: 16 April, 10am - Git part practice
- [ ] reboot camp OS module: 25 April 


<br>

---

## Feedback
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

