:::info
- Date/time: Tuesday 19 Mar 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rJ7I7Y4Ta/edit)
- Last meeting notes: [05 Mar 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HJgmhNf2p/edit)
- highlighted: ==important action items==
:::

# Open WIN Ambassadors meeting - 19 Mar 2024

## Agenda
1. Preparing reboot camp module


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
3. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
8. Juju Fars      / they/them / NDCN      / @pfr545   / 0000-0001-7771-5029
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----


## 0. Logistics
**Grad course / Reboot camp**
- Thu, 25 April, 10:00-13:00
- FMRIB annexe, Cowey room
- organised by: Kamila Szulc-Lerch, Tom Okell
- about half/half lectures and practicals
- scope for this module:
    - recap of essential fundamentals
    - more in-depth talks on selected open science topics
    - intermediate git 
    - we’ll need about 3-5 presenters/tutors for talks + practicals
- *Note: Bernd away 18 April - 06 May*


## 1. Resources and material
- resources:
    - [existing OS presentations](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/presentations?ref_type=heads)
    - [existing git presentations](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/GitLab%20training?ref_type=heads)
    - [additional material -- gitlab issue](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/18)
- main [gitlab repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/Open-science-training) for OS part


## 2. Open science part
*Main Idea*: going through the full life cycle of a project (incl. individual steps, data, code, ethics, licence, publishing, paper, etc.)
1. Lara: overview, recap of OS issues
2. Juju: (fake) research idea, ethics
3. Anna: preregistration, OSF, analysis ideas, code for pilot data
4. Lilian, Juju: MR protocol
5. Lilian: data sharing, code sharing (with / without comments), task sharing, readme
6. Juju: licencing and copyrights (brief)
7. Lilian, Lara: preprints, open access publishing

- MR protocol & database
    - [example protocol?](https://open.win.ox.ac.uk/protocols/stable/6974395a-3745-4861-b8cc-1887e787d1c4)

==**Next Steps**==
- have one slidedeck throughout
- do a practice run beforehand (especially w.r.t. timing)
- 


## 3. Git part
- main presenters: Juju, Ying-Qiu
    - tutor / practical help: Lilian
    - Ying-Qiu: command line
    - Juju: interactive git clients
- potential topics:
    - recap of basic commands
    - branching
    - pull/merge requests
    - rebasing
    - merge confilicts
    - good vs bad practices
    - ??
- practical:
    - short presentation on basic commands
    - start with an (empty?) shared remote repo
    - show merge and pull actions
    - demonstrate use of issues on github
    - create e.g. a readme file
    - create a new branch

==**Next Steps**==
- first draft structure 
- 


## 4. Other things
- [x] Lara: WIN Mon messages, gitlab access?
- [x] Anna: OS ambs video
- [ ] other ?

---

## ==Actions==
- [ ] ❗️preparing material for reboot camp❗️
- [ ] see gitlab issues
- [ ] collecting posters from various groups

<br>

## Upcoming
- [ ] Next meeting: 26 March, 10am ?
- [ ] reboot camp OS module: 25 April 


<br>

---

## Feedback
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

