:::info
- Date/time: Tuesday 09 July 2024, 10:00-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HJvzQdqPC/edit)
- Last meeting notes: [11 June 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rJDCe5SSC/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 09 July 2024

## Agenda
1. Admin
2. Updates
3. Other things?


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789


## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Admin



## 2. Updates

### Meeting with Iske & Kaitlin
- postponed to Tue, 16 July, 2-3pm

### Video
- done: Juju, Yingshi, Anna, Ying-Qiu
- remaining: Lilian, Lara, Bernd 

### Community pages
- more updates needed

### Thomas Willis Day
- Anna was there
- PE session -- new dedicated NDCN contact person
- nothing open science related

### WIN Mon messages
- Lara

### Next hack day?
- September



---

## ==Actions==
❗️ see gitlab issues: [ambassadors repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues) and [community repo](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues)


<br>

## Upcoming
- [ ] Next meeting: 
- [ ] 07 Aug: WIN Wed on preregistration

<br>

---

## Feedback
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

