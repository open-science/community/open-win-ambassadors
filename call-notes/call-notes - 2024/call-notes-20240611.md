:::info
- Date/time: Tuesday 11 June 2024, 09:00-15:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rJDCe5SSC/edit)
- Last meeting notes: [04 June 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/B104kjlmA/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors hack-day - 11 June 2024

## Agenda
1. Planning for upcoming weeks/months


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
3. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
8. Juju Fars      / they/them / NDCN      / @pfr545   / 0000-0001-7771-5029
9. Yingshi Feng   / she/her / NDCN        / @yingshif / 0000-0001-9065-4945
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Hack-day

### Open access oxford links to include on the community pages
- Open access (including open access publishing guide at Oxford:) https://openaccess.ox.ac.uk/oa-publication#collapse4321161
    - How to pay for open access: https://openaccess.ox.ac.uk/oa-payments
- Licenses: includes, what is a license, choosing a license, overview of licenses etc https://openaccess.ox.ac.uk/licences
- 

### Update parent sections
- Publishing 
    - Why publish in an open access journal 
    - Open access journals, Pre-prints, pre-registered reports 
    - Requirements by the funder 


### New suggested parent sections:
- Licensing
    - Why to license your work 
        - This link to open access oxford includes all the relvant information (https://openaccess.ox.ac.uk/licences)
    - What to license (publications, data, code etc)
    - Overview different licenses
    - Recommended University license
- Pre-registrations/registered reports
    - Why preregister my study?
    - Overview of how to preregister
    - What is the difference between a preregistration and a registered report?
    - Video from workshop with Jackie Thompson
- Step-by-step guide/workflow

    - Planning
        - Funding requirments/ grant applications 
            - [Open data](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/can-i/#governance)
            - [Data management plans](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/data-management-plans/)
        - Ethics , DPIA 
            - [Open data > Can I share my data > Ethics](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/can-i/#ethics)
            - [Open data > Governance](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/can-i/#governance)
        - [Pre-registration](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/preregistration/)
    - Data collection
        - [Experimental materials: MR protocol ](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/protocols/)
        - [Tasks](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tasks/)
        - [Data management ](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/data-management-plans/)
    - [Analysis ](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/analysis/)
        - [Git tutorials](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/gitlab-tutorials/)
    - [Disseminating and Sharing](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/publishing-licencing/)
        
    - [How to get help?](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/)
            -    open@win.ox.ac.uk


----

## Open Science Ambassadors video

Ideas:
    - Juju: what you knew about Git motivated them to join, and what they have learnt
    - Lilian: By being an ambassadors you gain insights into how people at the WIN are already collaborating/the infrastructure. At the same time, you also realise what are the challenges in open science and what still needs to be figured out.
Personally feels passionate about EDI, how do we make science more accessible?
    - Yingshi: wanted to learn about open science practices. Decided to join in her first year.
    She would like her research to be more reproducible and clear. What she realised after was that there was still a lot to be done in terms of infrastructure for sharing preclinical resources.
    - Anna: 
        - Why do open science?
        - What does it mean to be an Open WIN Ambassador? 
        - What motivated you to join the Open WIN Ambassadors community?
        - What have you learned since you became an open WIN ambassador?
        - How do you want to make a difference?
- People involved: Bernd (organiser), Yingshi (Year 1), Juju (Year 2), Lilian, Lara, Ying-Qui, Anna (current)
- Interested in learning more? Contact open@win.ac.ox.uk / link to community pages

----

## Chat with Stuart

- WIN Wed on licencing + DOI's
    - incorporate ORA's tool (Jason?)
- central focus: XNAT 
- Analysis pipeline: check in with Taylor, Paul, Fidel, etc. 
- OA publishing: check with Iske, Kaitlin
- Open tasks: 
    - need to advertise and raise awareness across WIN
    - doesn't have DOI creation functionality
    - idea: create a list of tasks that links to zenodo (with a date when task was uploaded)?
        - include published papers
    - more guidance on good readme files
        - showing examples for different scenarios
    - get in touch with Lawrence
