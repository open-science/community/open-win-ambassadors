:::info
- Date/time: Tuesday 08 Oct 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rk_wkdp0A/edit)
- Last meeting notes: [17 Sep 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/SJEYAdBTR/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 08 Oct 2024

## Agenda
1. Admin
2. Updates and catch-up
3. Other things?


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
3. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Admin


## 2. Updates

### Recap - meeting with Research Services (03 Oct)
- Roberta Burtsal, Iske Bakker, Bernd
- strong interest from central research services about how WIN is supporting researchers via the OS ambassadors programme
- see Iske's email: *The OS ambassadors' work is being noticed!*


### Ambassadors Video
- Anna started editing
- aiming for end of October


### Planning - WIN Wed seminar: hands-on MRI protocol upload
- *who:* ==Lilian, Anna, Yingshi?==
- *when:* 20 November, about 1/2h
- *what:*
    - bring your own protocol and we'll guide you through all steps to put it on the protocols database
    - content and planning after 08 Nov
- *admin:*
    - ask PI's to encourage attendance
    - prepare email before with instructions of what to bring!
    - combine with a WIP
    - confirm date with Iske (Anna)


### EEG database at WIN
- *who:* Anna, Lilian
- *what:*
    - how do groups across WIN currently handle EEG projects?
    - how to put EEG data on XNAT
    - guidelines for researchers
- *next steps*:
    - meeting Seb Rieger and Sven Braeutigam on 12 Oct to discuss


### Next hack day?
- *when:* 12 November!
- *where:* FMRIB Annexe
- *who:* all
- *what:*
    - community pages
    - EEG database (?)
    - other projects (?)


### Applications for new ambassadors 2025
- application period: 01-30 Nov
    - [ambassador programme info on community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/ambassadors/)
    - [ ] please have a look and let me know any comments/suggestions/etc. - ==all==
    - [ ] add to application questions: no prior experience required!
- start promoting / encouraging people to apply:
    - chat to groups, friends, PI's, etc. - ==all==
    - WIN Welcome session (23 Oct, 1-2pm, Cowey Room) - ==Bernd==
    - Mon newsletter - ==Lara==
    - FMRIB common room slide
    - Video!
    - Poster ?

    
### WIN ambassadors celebration half-day, workshop & dinner
- *when:* probably end of January
- *where:* ??
- *who:*
    - all current, previous, incoming ambassadors
    - some WIN management/admin folks
    - externals 
    - ??

### Graduate course - reboot camps 2025
- *when:*
    - reboot camp I: Jan 20 - Jan 24
    - reboot camp II: Apr 28 - May 02
- *where:* FMRIB Annexe
- *who:* 
- *what:*
    - Juju doing the first week intro to the grad-course 
    - need to think about when / what 

---

## ==Actions==
❗️ see gitlab issues: [ambassadors repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues) and [community repo](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues)


<br>

## Upcoming
- [ ] Next meeting: 29 Oct


<br>

---

## Feedback
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

