:::info
- Date/time: Tuesday 05 Mar 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HJgmhNf2p/edit)
- Last meeting notes: [20 Feb 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HyY4Nbcop/edit)
- highlighted: ==important action items==
:::

# Open WIN Ambassadors meeting - 05 Mar 2024

## Agenda
*All about grad-course reboot camp.*
1. Overview of existing presentation(s) 
2. Structure and content for the module 
3. Tasks and responsibilities 


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
3. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
8. Juju Fars      / they/them / NDCN      / @pfr545   / 0000-0001-7771-5029
9. Yingshi Feng   / she/her / NDCN        / @yingshif / 0000-0001-9065-4945
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----


## 0. Logistics
**Grad course / Reboot camp**
- Thu, 25 April, 10:00-13:00
- FMRIB annexe, Cowey room
- organised by: Kamila Szulc-Lerch, Tom Okell
- about half/half lectures and practicals
- audience: about 20 students on WIN’s grad-course
- hybrid format
- similar module last term has introduced open science ideas in general as well as the basics of git
- scope for this module:
    - recap of essential fundamentals
    - more in-depth talks on selected open science topics
    - intermediate git 
    - we’ll need about 3-5 presenters/tutors for talks + practicals
- *Note: Bernd away 18 April - 06 May*

## 1. Existing presentations and resources
- resources:
    - [existing OS presentations](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/presentations?ref_type=heads)
    - [existing git presentations](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/GitLab%20training?ref_type=heads)
    - [additional material -- gitlab issue](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/18)

==BRAINSTORMING==
- What topics (that can be covered in 15-20mins) would be most useful to be included in the module?
    - general overview of issues around open science practices
    - Data sharing? - existing platforms (WIN's protocols database, NeuroVault, OpenNeuro, Ebrains, etc.; BIDS data format)
    - Licensing/Copyright?
    - Preregistration/OSF

- What open science topics could be handled as a practical? (not git related)
    - going through the full life cycle of a project (incl. individual steps, data, code, ethics, licence, publishing, paper, etc.)
    - Juju: ethics, code
    - Anna: behavioural data, OSF, preregistration
    - Yingshi: MR protocol & database
    - Lilian: EEG & code
    - anyone for MR data sharing?


## 2. Structure and content for module
- overall structure:
    - 15-20mins: recap and overview of issues in OS in general
    - 80mins: step-by-step life cycle of a research project (4 x 20min sections?)
    - 15min break (after 1h30mins)
    - 1h git practical

**A) Open science part**
- certain topics in more detail (e.g. licencing, grant requirements, ...)


**B) Git part**
- intermediate / more advanced than just basics
- potential topics:
    - recap of basic commands
    - branching
    - pull/merge requests
    - rebasing
    - merge confilicts
    - interactive git clients (git desktop, git kraken, etc.)
    - ??
- practical:
    - short presentation on basic commands
    - start with an (empty?) shared remote repo
    - show merge and pull actions
    - demonstrate use of issues on github
    - create e.g. a readme file
    - create a new branch


## 3. Tasks and responsibilities
- presenters / tutors:
    - [ ] ..
    - [ ] ..

- ==creating presentations & practicals:==
    - [ ] Lara: overview of issues (?)
    - [xx] Juju: code, ethics, MR protocol 
    - [ ] Anna: prereg, OSF, behavioural data sharing
    - [ ] Lilian: code, EEG data
    - [ ] Yingshi: MR protocol database
    - [ ] GIT: Lilian, Juju, Ying-Qiu (?)


## 4. Other things
- [ ] hands-on session: 12 March, 12-2pm

---

## ==Actions==
- [ ] preparing material for reboot camp
- [ ] see gitlab issues
- [ ] collecting posters from various groups

<br>

## Upcoming
- Next meeting: 12 Mar 2024 -- info session & lunch at FMRIB Annexe, Cowey room


<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

