# Open WIN Ambassadors meeting - 09 Jan 2024

**General information**

- Date/time: Tuesday 09 Jan 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/BkEZOF5da/edit)
- Last meeting notes: [12 Dec 2023](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/B1OXrYhrT/edit)
- highlighted: ==important action items==

-----

## Agenda
1. Happy New Year!
2. MR protocols db poster
3. Planning OS ambassadors event (24 Jan)
4. WIN reboot camp (18 Jan)
5. Any other updates?

## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
3. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
9. Juju Fars      / they/them / NDCN      / @pfr545   / 0000-0001-7771-5029
12. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Happy New Year!


## 2. MR protocols db
- Create a poster for the scan room 
- see [gitlab issue 34](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/34)
    - Lilian's draft
    - Juju (?)
- deadline: 16 Jan


## 3. Ambassadors event (24 Jan)
- preliminary schedule:
    - 13:45 arrival
    - 14:00 start (2 separate tracks, open science and PE)
    - 14:05 ice-breaker round
    - 14:20 short talks (Jackie Thompson, Sarah Humphrey, Ruth Mallalieu, Sarah Callaghan)
    - 15:10 group discussion (all)
    - 15:40 fireside chat (Cass Gould van Praag)
    - 16:00 break and refreshments
    - 16:30 joint session (OS + PE)
    - 16:35 lightning talks (outgoing OS and PE ambassadors)
    - 17:00 future of PE and OS at WIN (Heidi Johansen-Berg, Tom Nichols)
    - 17:30 drinks reception
    - 18:00 dinner

- ==previous / outgoing ambassadors lightning talks:==
    - *Lisa, Mohamed, Juju, Yingshi, Verena*
	- 2min each (hard limit)
	- 2 slides max
	- what open science activity(ies) have you done this year (have you enjoyed working on)
	- what have you learned (what surprised you, etc.)
	- one piece of advice for incoming ambassadors

- ==hosting / chairing sessions:==
    1. ice-breaker round: Anna
        - facilitating brief introductions
        - maybe ask everyone to bring a photo of something? (e.g. "something that you associate with open science"?)
        - "photo of something you like / your favourite thing"
        - "something that relates to your hobbies"
    2. short talks: Ying-Qiu
        - introducing speakers
        - keeping time / making sure speakers don't overrun
    3. group discussion: Lara
        - guiding discussion
                - maybe start with questions regarding talks before
        - maybe interactively? (e.g. with [mentimeter.com](https://www.mentimeter.com/))
    4. fireside chat: *Bernd*


## 4. WIN reboot camp (15-19 Jan)
- schedule for Thu, 18 Jan:
    - 1000: intro to pandas (Michiel)
    - 1100: coffee break
    - 1115: intro to open science (Juju)
    - 1200: intro to gitlab (Bernd?)
    - 1300: end


## 5. Other things
- WIN newsletter contribution?
    - deadline: 15 Jan
    - collection of previous OS articles on gitlab: [newsletter items](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/comms/newsletter?ref_type=heads) (for reference) 
    - introducing new ambassadors

- photos + blurb for [webpage](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/ambassadors/meet_ambassadors/): Lilian?
    - [x] finally provided this (slack) - sorry!


## Actions
- [x] Bernd to ask WIN admin to add Lara to WIN newsletter email list
- [x] Bernd to send collection of blurbs + newsletter info to Anna
- [x] Bernd to ask IT about including new ambs on open WIN email account
- [ ] Bernd to add all photos and blurbs to ambs website
- [ ] Juju to prepare WIN reboot camp session and coordinate with Bernd, Peter (?)
- [ ] Anna to prepare WIN newsletter article and send to Jacqueline Pumphrey
- [ ] Juju and Lilian to finalise MR protocols poster

<br>

## Upcoming

- [x] Next meeting: 16 Jan 2024
    - finalise planning for event on 24th
- [ ] handing over comms to new ambassadors (incl. WIN Mon messages, twitter, PE contacts, etc. )
- [ ] after Jan 24 event: more specificly open science topics / tasks / projects


<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*
