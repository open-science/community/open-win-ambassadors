:::info
- Date/time: Tuesday 16 Jan 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rk5jpOZFa/edit)
- Last meeting notes: [09 Jan 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/BkEZOF5da/edit)
- highlighted: ==important action items==
:::

# Open WIN Ambassadors meeting - 16 Jan 2024

## Agenda
1. MR protocols db poster
2. OS ambassadors event (24 Jan)
3. Any other updates

## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
6. Lisa Spiering  / she/her  / Exp Psych. / @wym003   / 0000-0002-9071-6541
11. Yingshi Feng  / she/her / NDCN        / @yingshif / 0000-0001-9065-4945
12. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. MR protocols db
- Goal: Create a poster for the scan room 
- see [gitlab issue 34](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/34)
- deadline: 16 Jan

## 2. Ambassadors event (24 Jan)
- preliminary schedule:
    - 13:45 arrival
    - 14:00 start (2 separate tracks, open science and PE)
    - 14:05 ice-breaker round
    - 14:20 short talks (Jackie Thompson, Sarah Humphreys, Jason Partridge / Ruth Mallalieu, Sarah Callaghan)
    - 15:10 group discussion (all)
    - 15:45 fireside chat (Cass Gould van Praag)
    - 16:00 break and refreshments
    - 16:30 joint session (OS + PE)
    - 16:35 lightning talks (outgoing OS and PE ambassadors)
    - 17:00 future of PE and OS at WIN (Heidi Johansen-Berg, Tom Nichols)
    - 17:30 drinks reception
    - 18:00 dinner

- ==previous / outgoing ambassadors lightning talks:==
    - confirmed: 
        - [x] Lisa
        - [x] Mohamed
        - [x] Juju
        - [x] Yingshi
        - [x] Verena
	- 2min each (hard limit)
	- 2 slides max
	- what open science activity(ies) have you done this year (have you enjoyed working on)
	- what have you learned (what surprised you, etc.)
	- one piece of advice for incoming ambassadors

#### ==hosting / chairing sessions:==
- key responsibility: keeping an eye on time
1. ice-breaker round: *Anna*
    - facilitating brief introductions (e.g. name, current role + 1-2 sentences about a photo/thing)
    - "For the ice-breaker round, please could you bring a picture which is special to you, and be prepared to say one or two sentences about it. This is just a little exercise to help us get to know each other. It could be a picture of anything which represents something to you, e.g., a place, an object, a person, etc. Please send this picture to Bernd Taschler by midday on the 24th Jan."

2. short talks: *Ying-Qiu*
    - introducing speakers & keeping time / making sure speakers don't overrun
    - Jackie Thompson:
    • UK Reproducibility Network Open Research Project Officer
    • part of the Bodleian Libraries’ open scholarship support team
    • her research interests are focused on meta-research projects, for example, evaluating interventions to improve funding and publishing systems, and better understanding what responsible research means across disciplines.

    - Sarah Humphreys:
    • Senior Library Assistant at The Bodleian Libraries
    • experience from previous roles at Berkshire Healthcare NHS Foundation Trust, The National Archives, and University of Reading. 

    - Jason Partridge:
    • Open Access Service Manager at the Bodleian Libraries
    • Responsible for the Bodleian’s Open Access Collections including management of the addition of content into Oxford’s open access repository, ORA (Oxford University Research Archive) and ORA Data, and the management of the payment of Article Processing Charges.

    - Sarah Callaghan: 
    • Research Practice Manager at the central university’s research services
    • until 2022 was editor-in-chief at Patterns, a CellPress journal
    • Sarah has a 20-year career in creating, managing, and analyzing scientific data. Her research started as a combination of radio propagation engineering and meteorological modelling, then moved into data citation and publication, visualization, metadata, and data management for the environmental sciences. 
    
3. group discussion: *Lara*
    - guiding discussion (maybe start with questions regarding talks before?)
    - maybe interactively? (e.g. with [mentimeter.com](https://www.mentimeter.com/)) -- opening poll?
    - e.g. what has people stopped / incouraged to take up open science practices?
    - e.g. is there a trade-off between OA journals vs high-impact journals?
    - e.g. what could WIN do in the next year or so to further increase open science profile?
    - e.g. existing barriers to sharing research outputs (different formats, coding abilities, work in progress)
    - reserving some time for open questions / spontaneous ideas on the day

4. fireside chat: *Cass & Bernd*


## 3. Other things
- [x] WIN reboot camp
    - [detailed programme](https://www.win.ox.ac.uk/files/training/2023_24_reboot_camp_i_schedule.pdf)
    - all sessions are run in hybrid format
    - if you'd like to attend any of the session please email graduate_admin@fmrib.ox.ac.uk 
- [ ] Lara: WIN IT account and mailing list?
- [x] WIN newsletter contribution
- [x] photos + blurbs live on [webpage](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/ambassadors/meet_ambassadors/)
- [x] ... ?


## Actions
- [ ] send poster to Stuart
- [x] Anna to write a short prompt for ice-breaker round
- [ ] ask Hanna Smyth to send out programme and prompt for ice-breaker
<br>

## Upcoming

- Next meeting: 23 Jan 2024
    - any last-minute issues before event
- handing over comms to new ambassadors (incl. WIN Mon messages, twitter, PE contacts, etc. )
- after Jan 24 event: more specificly open science topics / tasks / projects


<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

