:::info
- Date/time: Tuesday 06 Feb 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/SytAAfYqT/edit)
- Last meeting notes: [16 Jan 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rk5jpOZFa/edit)
- highlighted: ==important action items==
:::

# Open WIN Ambassadors meeting - 06 Feb 2024

## Agenda
1. OS ambassadors event (recap)
2. WIN OS steering group meeting (recap)
3. Ambassadors training ideas
4. Handing over of comms channels
5. Any other updates

## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
8. Juju Fars      / they/them / NDCN      / @pfr545   / 0000-0001-7771-5029
10. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Ambassadors event (recap)
- feedback / how did it go?
    - very positive feedback from external speakers
    - maybe fewer, more in depth talks next time
    - maybe include a discussion session amongst ambassadors


## 2. WIN OS steering group meeting (recap)
- 30 Jan 2024
- Stuart Clare, Tom Nichols, Duncan Mortimer, Duncan Smith, Clare Mackay, Bernd
- data sharing:
	- pilot project with Miriam Kleinflügge to transfer data to BMRC servers (DSAA issues)
- brain health clinic:
	- non-imaging subject data sits behind NHS firewall
	- if anonymised, data could travel to university side for research
	- issue: current workflow is fragile (essentially still a prototype)
- MR protocols db:
	- visibility: Duncan M to implement a button "show all open protocols" 
	- will automatically be indexed on google, etc.
- ORA (Jason Partridge):
	- integrating MR protocols, WIN's data, software into ORA ? 
    - Tom to set up a meeting with IT (WIN, Bodleian) to discuss technical possibility
	- organise a WIN Wed?
- suggestion to make a short video: "What does it mean to be an OS ambassador?"
    - using [canva](https://www.canva.com/video-editor/)?
- should we axe the OS slack space ?
    - open a separate channel on the EDI slack space
    - maybe rename EDI as "WIN community"? -- discuss with Amy, Karla

## 3. Ambassadors training
- in-person session (about 2h)
    - with Stuart, Duncan M
    - going through resources, background info, etc.
    - OS best practices
- software carpentries
    - version control with git
    - programming (R, python, shell)
    - see overview of [available lessons](https://software-carpentry.org/lessons/)
    - [becoming an instructor](https://carpentries.org/become-instructor/)
- [RROx events and workshops](https://ox.ukrn.org/events/)
- jupyter labs


## 4. Handing over of comms channels and other projects
- WIN Monday messages
    - [x] previously: Peter
    - [x] new: Lara -- also on the NDCN email list
- twitter:
    - [x] previously: Juju
    - [ ] new: *postponed for now*
    - [resources](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/blob/master/presentations/Introduction_to_Open_Science_-_Part_2.pptx)
    - is twitter still relevant?
    - possible alternatives: "threads" or "bluesky"?
- resources on gitlab: [main comms directory](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/comms?ref_type=heads)
    - incl. drafts, some general info, etc.
- updating community pages
    - in small chunks / by section
    - [gitlab issues](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues)
    - how best to divide tasks?
    - get an external point of view (e.g. from RROx)

## 5. Other things
- [ ] Lilian: how did WIN welcome session go?
- [ ] Lara: WIN IT account and mailing list?
- [ ] Nicole's email
    - organise a success story talk at a WIN Wed?
- [ ] project idea: look into how/where to share MRI data according to ethics (OA but not comercially)
- [ ] idea: create a poster on steps to publish open access


---

## ==Actions==
- [ ] get feedback on MR protocols poster from Stuart
- [ ] Anna to lead video production 
- [ ] Lara to take over WIN Mon messages 
- [ ] Ying-Qiu to start updating community pages
- [ ] Bernd to follow-up with Xenia on ebrains
- [ ] Bernd to create list of sections on community pages that i) need updating, ii) need more content
- [ ] Bernd to discuss with Amy, Karla, Stuart about integrating OS slack space into EDI space
- [ ] Bernd to coordinate a date for in-person session with ambs, Stuart, Duncan
- [ ] Lara to check access to gitlab
- [ ] organise a WIN Wed around ORA (Jason Partridge)

<br>

## Upcoming
- Next meeting: 13 Feb 2024



<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

