:::info
- Date/time: Tuesday 13 Feb 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/ryIctM-jT/edit)
- Last meeting notes: [06 Feb 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/SytAAfYqT/edit)
- highlighted: ==important action items==
:::

# Open WIN Ambassadors meeting - 13 Feb 2024

## Agenda
1. Project updates
2. Gitlab / Editing community pages
3. Any other updates

## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
8. Juju Fars      / they/them / NDCN      / @pfr545   / 0000-0001-7771-5029
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Project updates
**WIN Monday messages** (Lara)
- email win-admin@ndcn.ox.ac.uk to add messages
- Jackies email regarding research practice survey

**Short video** (Anna)
- intro to what is open science
- making use of ambassador statements

**Grad course / Reboot camp**
- start of trinity term: 22 April
- organised by: Kamila, Tom Okell
- 1. OS: Lara, Anna
- 2. git: 
- both: Ying-Qiu, Juju


## 2. Gitlab: editing the community pages
- [gitlab issues](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues)

    
## 3. Other things
- [ ] Lara: gitlab access?
- [ ] how to share large data - now that one drive may not be an option anymore
- [ ] WIN Wed ideas:
    - registered reports (e.g. Jackie Thompson)
    - negative findings / filedrawer problem

---

## ==Actions==
- [ ] get feedback on MR protocols poster from Stuart (*chasing Stuart*)
- [ ] Ying-Qiu to start updating community pages
- [ ] Bernd to create a list of sections on community pages that i) need updating, ii) need more content
- [ ] Bernd to discuss with Amy, Karla, Stuart about integrating OS slack space into EDI space
- [ ] Bernd to coordinate a date for in-person session with ambs, Stuart, Duncan (*chasing Stuart*)
- [ ] organise a WIN Wed around ORA (Jason Partridge)
- [ ] *all* to think about content for reboot camp / grad-course 

<br>

## Upcoming
- Next meeting: 20 Feb 2024 -- start at 09:30?

<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

