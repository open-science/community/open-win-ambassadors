:::info
- Date/time: Tuesday 03 Dc 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/Hk574Ii7ye/edit)
- Last meeting notes: [12 Nov 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/Hyb1Hu9y1x/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 03 Dec 2024

## Agenda
1. Admin
2. Wrap-up of 2024
7. Other things?


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
3. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
4. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789


## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Admin
- recap WIN Wed on MRI protocol upload
- likely 4 new ambassadors
- 


## 2. WIN Welcome day
- Wed, 15 Jan, 1-2pm 
- 5-10 min presentation on OS at WIN 
- slides are alreday done -- [slidedeck](https://unioxfordnexus.sharepoint.com/:p:/r/sites/WIN-HUB/WIN%20Management%20Board/WIN%20Talks/WIN_Welcome_2024.pptx?d=wdcd6f6c3cc9f4373b8768f306944e46b&csf=1&web=1&e=i1oDxG)
    - *if you don't have access, email Eugenie or let Bernd know*
- probably sandwich lunch provided


## 3. WIN ambassadors day
- Wed, 29 Jan
- [registration](https://app.onlinesurveys.jisc.ac.uk/s/oxford/registration-29-jan-2025-ambassadors-event)
- short talks: 
    - [ ] Anna
    - [ ] Lara
    - [ ] Ying-Qiu
    - ~~Lilian~~
    - [ ] Juju
    - [ ] Yingshi


## 4. Planning reboot camps 2025
- reboot camp 1: 
    - *when*: Jan 20 - 24
    - *what*: git / OS
        - more of an overview
        - preregistration and anything that should happen early in a project
        - licensing and OA publishing is most confusing topic
        - include a discussion section (after 1h of overview) about concrete projects that students are working on
        - need enough time for practical -- maybe shift to camp 2
    - *who*: Lara, Anna 
- reboot camp 2: 
    - *when*: Apr 28 - May 02
    - *what*: git / OS
    - *who*:
- TODO:
    - [ ] see existing resources / last year's material: [gitlab](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors)
    - [ ] prepare tutorial slides
    - [ ] prepare practicals
    - [ ] check exact time and date with Kamilla 


### Last year's reboot camps
**reboot camp 1 (2h):**
- Part 1: Intro to open science (Juju)
- Part 2: git practical (Bernd)


**reboot camp 2 (3h):**
- Part 1: How does open science fit into the lifecycle of a research project?
    - overview, recap of most common OS issues - (Lara)
    - research idea and ethics - (Juju)
    - preregistration, OSF, analysis ideas, code for pilot data - (Anna)
    - data sharing, code sharing, task sharing, readme files - (Lilian)
    - licensing and copyrights - (Juju)
    - preprints, open access publishing - (Lara / Lilian)
- Part 2: Git and GitHub practical - (Ying-Qiu and Juju) 
    - recap of basic commands and git workflow
    - working with an online repository
    - branching
    - pull/push and merge requests
    - creating issues and other Github functionalities


## 5. Wrapping up 2024
*A very big thank you to all of you!*
![thank_you](https://hackmd.io/_uploads/Hy0KmU2m1x.jpg)


Would you like to stay involved in OS projects at WIN? ... If yes, to what extent? / In what capacity?
- *Anna* -- EEG database, general things, communications (e.g. Bodleian)
- *Ying-Qiu* -- git training
- *Lara* -- e.g. OS teaching
- *Yingshi* -- reduced but in for socials
- *Lilian* -- reduced, maybe only for first half of year (esp. EEG database)


---

## ==Actions==
- [ ] ..
- [ ] ...


<br>

## Upcoming
- [ ] xmas :) 
- [ ] Next meeting: 07 Jan? (together with new ambassadors)


<br>

---

## Feedback
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*
