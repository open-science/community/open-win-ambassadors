:::info
- Date/time: Tuesday 20 Feb 2024, 09:30-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HyY4Nbcop/edit)
- Last meeting notes: [13 Feb 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/ryIctM-jT/edit)
- highlighted: ==important action items==
:::

# Open WIN Ambassadors meeting - 20 Feb 2024

## Agenda
1. Project updates
2. WIN Wednesday talks
3. Updating community pages
4. Any other updates

## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
3. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
8. Juju Fars      / they/them / NDCN      / @pfr545   / 0000-0001-7771-5029
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Project updates
**WIN Monday messages** (Lara)
- email win-admin@ndcn.ox.ac.uk to add messages
- [x] Jackies email: research practice survey
- [x] upcoming week(s)

**Short video** (Anna)
- [ ] intro to what is open science
- [ ] making use of ambassador statements
- [ ] [issue on gitlab](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/35)

**Grad course / Reboot camp**
- start of trinity term: 22 April
- organised by: Kamila, Tom Okell
- 2 parts: open science in general & git
    - OS: Lara, Anna
    - git: 
    - both: Ying-Qiu, Juju, Lilian
- material:
    - [presentations on gitlab](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/presentations?ref_type=heads)
    - [additional material -- gitlab issue](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues/18)
    - for git: more advanced than just basics
    - for OS: certain topics in more detail (e.g. licencing, grant requirements)

**Poster showcase/gallery**
- hosted on community repo
- implementation: working on a separate branch until ready
- start collecting posters!
    - talk to people directly and ask for poster
    - ask during/after events
    

## 2. WIN Wed talks
- lead: ??
- organising and hosting open science related WIN Wed seminars
- potential speakers / topics:
    - Jason Partridge: ORA
    - Jackie Thompson: preregistered reports (?) -- *Anna*
    - something around negative findings / "filedrawer problem"
    - open science success stories (*Lisa*)
        - e.g. Nicole (?)


## 3. Updating the community pages
- [gitlab issues - ambassadors repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues)
- [gitlab issues - community repo](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues)
- especially, data sections need updating

    
## 4. Other things
- [ ] Lara: gitlab access?
- [ ] hands-on session: 12 March, 12-2pm
- [ ] project idea: look into how/where to share MRI data according to ethics (OA but not comercially)
- [ ] idea: create a poster on steps to publish open access
- [ ] how to share large data - now that one drive may not be an option anymore

---

## ==Actions==
- [x] Ying-Qiu to start updating community pages
- [ ] see gitlab issues
- [ ] *all* to think about content for reboot camp / grad-course 
- [ ] grad-course: go through existing presentations
- [ ] switch to bi-weekly meetings -- with more dedicated themes
- [ ] *all*: start collecting posters from others
- [ ] Anna to organise WIN Wed with Jackie

<br>

## Upcoming
- Next meeting: 05 Mar 2024
    - dedicated to grad-course


<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

