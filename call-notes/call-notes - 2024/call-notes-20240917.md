:::info
- Date/time: Tuesday 17 Sep 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/SJEYAdBTR/edit)
- Last meeting notes: [03 Sep 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HJ6z1_s90/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 17 Sep 2024

## Agenda
1. Admin
2. Updates and catch-up
3. Next steps
4. Other things?


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
3. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
8. Juju Fars      / they/them / NDCN      / @pfr545   / 0000-0001-7771-5029
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Admin
- Goodbye summer!
- keep Tue 10-11 as meeting time?
- every 2-3 weeks?


## 2. Updates

### Recap - WIN OS Management group meeting
- data sharing:
	- Oxford (central) has a very stringent interpretation of the law, requiring onerous DPIA's for almost anything --> from a legal point, much more lenient interpretations are possible
	- BMRC is only a "processor" of data --> i.e. legally, it doesn't need to police compliance
	- individual researcher is "controller" of data --> i.e. assumes responsibility
	- any biometric data (measures that can identify an individual) requires a DPIA -- e.g. genetics, fingerprints, brain folding patterns?
- protocols database is now searchable on google
    - ==engagement idea==: organise a WIN Wed hands-on "bring your protocol and upload it!"
    - ask PI's to encourage attendance
    - combine with a WIP
- EEG database at WIN:
    - no structured pipeline (as for MRI) available because acquisition is not via the same computer
	- Dave Flitney has developed a python tool that can import any data into XNAT --> requirement: existing project on calpendo
	- MEG at OHBA is already using XNAT
	- needs some guidelines explaining necessary steps --> *ambassador project?*
	- ==Anna, Lilian==

- plans for 2025:
    - 3 WIN ambassador cohorts: PE, DEI, OS
	- joint launch at a WIN Wed 
	- workshop / away-day in January
	- *We would love to have you on board for another year!!*


### Video
- Lilian, Lara, (Bernd) 

### WIN Newsletter
- see Stuart's email (16 Sep)
    - talk about summer school 
    - 
### Community pages
- updates ongoing

### Graduate course - reboot camps 2025
- (I): Jan 20-24
- (II): Apr 28 - May 02
- who?, what?
    - Juju doing the first week intro to the grad-course 
    - need to think about when / what 

### Next hack day?
- early / mid October
- please respond to the [doodle poll](https://doodle.com/meeting/participate/id/e3KZPvAe)


## 3. Focus / Activities / Projects for next few months
- *What would you like to work on?*
- *What project / activity would interest you most?*
- *What do you think is the most important / useful thing to focus on?*
- Anna: EEG BIDS -- overlap with fellowship


---

## ==Actions==
❗️ see gitlab issues: [ambassadors repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues) and [community repo](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues)


<br>

## Upcoming
- [ ] Next meeting: ??
- [ ] ??

<br>

---

## Feedback
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

