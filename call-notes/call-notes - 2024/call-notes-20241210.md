:::info
- Date/time: Tuesday 10 Dc 2024, 09:30-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/SyTmsteNJx/edit)
- Last meeting notes: [03 Dec 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/Hk574Ii7ye/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 10 Dec 2024

## Agenda
1. Welcome
2. Programme overview
3. Admin
4. Discussion


## Participants


| Name                | pronouns | department | WIN gitlab ID | ORCID |
| ------------------- | -------- | ---------- | ------------- | ----- |
| Kirralise Hansford  | She/her | Women's and Reproductive Health |   | 0000-0002-5738-6843 |
| Tara Ghafari        | She/her  |  TBC       | TBC           | 0000-0002-5178-8702 |
| Yifan (Vivian) Yang |she/her   | Experimental Psychology  | grx569  | 0000-0003-0211-1313 |
| Séamus O'Sullivan   | he/him | Psychiatry | fhv480 | n/a |
| Bernd Taschler      | he/him   | BDI        | @ndcn1032     | 0000-0001-6574-4789 |



## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Welcome intro
![welcome](https://hackmd.io/_uploads/BJCH6DEVJg.png)


## 2. Ambassadors programme - Overview

### A little history
- 2017: as part of the first WIN bid, one of WIN's themes was open imaging.
- All 15 or so WIN PIs agreed to share outputs and "document how we got there".
- The reproducibility crisis put open science into the spotlight.
- Translational value as motivating factor: why do we rarely get from clinical research to clinical practice?
- FSL has always been open and free to the academic community.
- The wider neuroimaging field has a strong open research culture.
- Challenges: initially, mostly technical challenges, but then came along ethical, governance, skills challenges, etc.
- **Vision Statement**: WIN will become an open science community with a positive culture for sharing data, tasks and tools, to improve transparency, reproducibility and impact ...
- Cass Gould van Praag joined WIN in a dedicated role as open science coordinator. 
- First ambassadors cohort: 2022
- As of 2023: some uncertainty about future (esp. funding), but WIN management very committed to keeping open science initiatives.
- 2025: "new" WIN centre, open science will remain a key aspect of research and culture and the centre

### Goals
- Output based: putting research outputs into a structure for others to access
- Project based: data can be selectively accessed to fit a requirement of a new project (e.g. participant characteristic).
- Sharing across different levels: setting up the infrastructure to enable everyone at WIN to share publicly when you are ready to do so.
- What do we need to do to support people to share their research? 
- How to incentivise open science best practices?

### Open science management team
- Clare MacKay (2017-2022)
- Tom Nichols (2023-)
- Stuart Clare (admin)
- Heidi Johansen-Berg (WIN head) 
- Duncan Mortimer (IT)

### Projects
- community pages
    - short redirect: www.win.ox.ac.uk/open ... have a look around!
    - everything is hosted via gitlab pages
    - ambassador's [gitlab repository](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors)
- WIN's [MR protocols database](https://open.win.ox.ac.uk/protocols/)
    - info on [community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/protocols/)
- EEG database (work in progress)
- outreach
    - WIN Monday messages, newsletter, seminars, training, talks, flyers, etc.
    - visibility: open science in research projects across WIN
- contacts to wider Oxford community
    - RROx, Bodleian, research services, etc.
- teaching and training courses
    - OS topics
    - git
    - WIN graduate-course reboot camps
- shared OS support email account: open@win.ox.ac.uk 
    - [ ] Bernd to update members list (once Tara has an Ox SSO)
- individual projects



## 3. Admin
- meeting time and dates
    - Wed 9.30-10.30
- hackmd and markdown
- Git and GitLab (github?)
    - [ ] Does everyone have a Gitlab account?
- slack channel as part of WIN community slack space
    - [ ] everyone joined
    - not main comms channel
    - [x] add a Teams channel
- photos and blurb for [profiles](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/ambassadors/meet_ambassadors/) on community pages
- WIN ambassadors day: 29 Jan
    - [x] registrations


## 4. Discussion
- What do you associate with "open science"?
- Do you have any good/bad experiences related to open science?
- What motivated you to join the open science ambassadors programme?
- What would you like to achieve as OS ambassador?
- What do you think are the most important/useful areas we could focus on?


---

## ==Actions==
- [x] Bernd to send follow-up email


<br>

## Upcoming
- [x] xmas :) 
- [x] Next meeting: 08 Jan (together with some previous ambassadors)


<br>

---

## Feedback
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*
