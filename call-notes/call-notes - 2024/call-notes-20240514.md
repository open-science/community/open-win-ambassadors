:::info
- Date/time: Tuesday 14 May 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HkKSWijGA/edit)
- Last meeting notes: [07 May 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HkYBb_5e0/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 14 May 2024

## Agenda
1. Planning for upcoming weeks/months


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
3. Lilian Weber   / she/her  / Psychiatry / @lilweber / 0000-0001-9727-9623
4. Ying-Qiu (Akina) Zheng / she/her / FMRIB / @yqzheng1 / 0000-0003-1236-0700
8. Juju Fars      / they/them / NDCN      / @pfr545   / 0000-0001-7771-5029
9. Yingshi Feng   / she/her / NDCN        / @yingshif / 0000-0001-9065-4945
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789

## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Ongoing Projects
- WIN Monday messages: *Lara*
- updating community pages: *Ying-Qiu*, *everyone* 
    - 1/2 day hackathon to work together on this
- ambassadors video: *Anna*, ...
    - find a Wednesday to meet up and record short videos (plus lunch?)
    - Niki Andrews (NDCN, level 6) could provide the camera
- poster repository
    - create space on community pages: as an additional logo/link on the starting page
    - short WIN Wed update on recent developments plus advertisment for poster repo
- WIN Wednesday seminars
    - idea: Daniel Baker (York) - project: reproduce me: https://osf.io/preprints/psyarxiv/k8d4u


## 2. Interesting Topics

*brainstorming* ....

- licensing & copyrights!
- course on digital notetaking: https://www.dannygarside.co.uk/open-notebook#/page/course%3A%20digital%20notetaking%20for%20researchers 
- EEG/MEG repositories https://github.com/openlists/ElectrophysiologyData
- GIN https://gin.g-node.org/G-Node/Info/wiki


(*please vote below by adding a "+" in the brackets next to a topic*)
- publishing: 
    - licensing and copyrights (++++)
    - DOI's ()
    - OA routes (+++)
    - funding requirements - Wellcome, UKRI, BRC, etc. (++++)  - who would be good to ask/invite (Iske M. - grant related)
- coding: 
    - unit tests ()
    - readme files (++)
    - version control / git (+)
    - documentation (+)
- data sharing:
    - open platforms for neuroimaging data (++)
    - how to share data? ()
    - what data can I share? ()
    - resources for EEG / MEG specifically - e.g. [this repository](https://github.com/openlists/ElectrophysiologyData) (++)
    - checking out [GIN](https://gin.g-node.org/G-Node/Info/wiki)? ()
- ethics:
    - DPA / DPIA ()
    - plagiarism (+)
    - genrative AI (+)
- research practices:
    - course on [digital notetaking](https://www.dannygarside.co.uk/open-notebook#/page/course%3A%20digital%20notetaking%20for%20researchers) (++)
    - inclusivity (++++++) -- e.g. invite Morgan Mitchell (or anyone else from DEI group)
    - lab handbooks (+)
    - hackathons (+)
- outreach:
    - community engagement ()
    - why OS / benefits of OS (+)
- ??


## 3. Other things
- [x] send reboot camp slides to Kamila
- [ ] Integrating Xenia's EBRAINS guide into community pages
- [ ] WIN Wed on pre-registration: August (*Anna, Lisa, Verena, Jackie Thompson*)
    - anyone who could show neuroimaging example of preregistered study??
- [x] WIN OS steering committee meeting: 16 May
    - ask about git/gitlab training plans -- maybe a joint session with IT?

---

## ==Actions==
- [ ] see gitlab: [issues - ambassadors repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues) and [issues - community repo](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues)
- [ ] create space on community pages (*Bernd*)
- [ ] collecting posters from various groups
- [ ] Lilian to reach out to Morgan about inclusivity (esp. EEG)
- [x] Bernd to reach out to Iske 

<br>

## Upcoming
- [ ] Next meeting: 28 May, 10am -- funders' requirements OR licensing
- [ ] Thomas Willis Day (NDCN away-day): 18 June

<br>

---

## Feedback
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

