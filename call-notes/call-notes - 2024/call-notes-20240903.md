:::info
- Date/time: Tuesday 03 Sep 2024, 10:00-11:00 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HJ6z1_s90/edit)
- Last meeting notes: [09 July 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HJvzQdqPC/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 03 Sep 2024

## Agenda
1. Admin
2. Updates and catch-up
3. Other things?


## Participants
(name             / pronouns / department / GitLab ID / ORCID)
1. Anna Guttesen  / she/her  /  FMRIB     / @nnm329   / 0000-0003-0284-1578 
2. Lara Nikel     / she/her  / NDCN       / @lara.nikel / 0000-
11. Bernd Taschler / he/him   / BDI       / @ndcn1032 / 0000-0001-6574-4789


## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Admin
- Welcome back!


## 2. Updates

### WIN Wed on preregistration with Jackie Thompson
- 07 August
- recording
- material and videos from Jackie / UKRN 
- "training the trainers": funding available from UKRN

### Video
- Lilian, Lara, Bernd 

### Community pages
- ongoing

### Graduate course - reboot camps 2025
- (I): Jan 20-24
- (II): Apr 28 - May 02
- who?, what?

### WIN Mon messages
- all good

### Next hack day?
- please respond to the [doodle poll](https://doodle.com/meeting/participate/id/e3KZPvAe)


### Focus / Activities / Projects for next few months
- *What would you like to work on?*
- *What project / activity would interest you most?*
- *What do you think is the most important / useful thing to focus on?*
- Anna: EEG BIDS -- overlap with fellowship
- central repository for EEG and MEG data (?)


---

## ==Actions==
❗️ see gitlab issues: [ambassadors repo](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/issues) and [community repo](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues)


<br>

## Upcoming
- [ ] Next meeting: 17 Sep ?
- [ ] ??

<br>

---

## Feedback
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

