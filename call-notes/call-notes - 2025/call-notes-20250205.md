:::info
- Date/time: Tuesday 05 Feb 2025, 09:30-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rkgfifcu1l/edit)
- Last meeting notes: [08 Jan 2025](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HkBV9tqIye/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 05 Feb 2025

## Agenda
1. Admin
2. Recap of WIN ambassadors event
3. Projects: next steps
4. Other things?


## Participants

| Name                | pronouns | department | WIN gitlab ID | ORCID |
| ------------------- | -------- | ---------- | ------------- | ----- |
| Kirralise Hansford  | she/her  | Women's and Reproductive Health | TBC | 0000-0002-5738-6843 |
| Tara Ghafari        | she/her  |  TBC       | @tara.ghafari | 0000-0002-5178-8702 |
| Vivian (Yifan) Yang | she/her  | Experimental Psychology    | @dpx745  | 0000-0003-0211-1313 |
| Séamus O'Sullivan   | he/him   | Psychiatry | @fhv480       | 0009-0009-1079-7379 |
| Anna Guttesen       | she/her  | FMRIB      | @nnm329       | 0000-0003-0284-1578 |
| Bernd Taschler      | he/him   | BDI        | @ndcn1032     | 0000-0001-6574-4789 |


## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Admin
- test new forwarding: open@win.ox.ac.uk to win_open@maillist.ox.ac.uk
    - send test email from a non-Ox account
    - check: e.g. does Tara receive it?
- change of regular meeting time:
    - Mon 9.30-10.30 (KH, YY, AG) or 10.00-11.00 (KH, YY, AG) or 15.30-16.30 (KH, YY, AG)
    - Tue 9.30-10.30 (AG, TG) or 13.30-14.30 (YY, AG, TG)
    - Wed 10.30-11.30 (YY, TG)
    - [x] Thu 9.30-10.30 (KH, YY, AG, TG)
    - Fri 11.00-12.00 (YY) or 14.30-15.30 (KH, TG)
- external OS mailing lists:
    - [OSF](https://osf.io/)
    - [RROx](https://rr.ox.ac.uk/get-involved)
- event: [OxFOS 2025](https://openaccess.ox.ac.uk/oxfos/)
    - online / in-person (Weston library)
    - all free to attend

## 2. Recap WIN ambassadors event
- any feedback?


## 3. Projects
### General
- next steps?
- additional info/support needed?
- meetings with other people needed?
- (if useful) use issues on gitlab to keep track of to-do items

### EEG database
- Dave Flitney has coded the tool that takes MEG data and puts it into XNAT
- **example project**
    -  ![image](https://hackmd.io/_uploads/HJH-HhxFJe.png)
    - ![image](https://hackmd.io/_uploads/H1kxVbbF1e.png)


- goal: get something similar for EEG data
- next steps
- set up a meeting: IT, Seb, Séamus, Anna, Lilian, Tara (example project on XNAT)

### MR protocols database
- issue anonymisation (blacked out pdf sections)
    - think about easiest way to give step-by-step guidance for users
    - think about how to deal with existing pdfs
- check: can it take protocols from old/external scanners?
    - add a paragraph explaining what to do if system doesn't support your protocol
- ways to increase uptake: loop in radiographers?
- Poster near MRI rooms

### WIN Monday messages
- Kirralise working on it

### Outreach / connections
- Bodleian
- other departments?
- Jason's email
    - e.g. anything for community pages

### Training / seminars / etc.
- success stories
- drop-in sessions
- reboot camp II (April
- themed-leader session:
    - pitch a short OS-session to PI's

### Data sharing
- Marieke Martens
- Ludo Griffanti
- meeting with Tom Nichols, Vivian, Marieke, Ludo, Bernd -- next week
- recent development on export licenses for sharing data overseas
    - check new rules!
    - meet with Stuart? (attended meeting with Kirralise)

### Rebranding of WIN
- need to think about what needs to change 
- check with Stuart
- any other things that should be cleaned up / changed in the process
    - use vector images




<br>

---

## ==Actions==
- [ ] ...


## Upcoming
- [x] Next meeting: 19 Feb 2025?
- [ ] hack day: ??


<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

