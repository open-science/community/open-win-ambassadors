:::info
- Date/time: Tuesday 20 Feb 2025, 09:30-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rk7jkbCu1g/edit)
- Last meeting notes: [05 Feb 2025](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/rkgfifcu1l/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 20 Feb 2025

## Agenda
1. Admin
2. Poster for OxFOS event
3. Project updates
4. Gitlab


## Participants

| Name                | pronouns | department | WIN gitlab ID | ORCID |
| ------------------- | -------- | ---------- | ------------- | ----- |
| Kirralise Hansford  | she/her  | Women's and Reproductive Health | TBC | 0000-0002-5738-6843 |
| Tara Ghafari        | she/her  | Experimental Psychology   | @tara.ghafari | 0000-0002-5178-8702 |
| Yifan (Vivian) Yang | she/her  | Experimental Psychology    | @dpx745  | 0000-0003-0211-1313 |
| Séamus O'Sullivan   | he/him   | Psychiatry | @fhv480       | 0009-0009-1079-7379 |
| Bernd Taschler      | he/him   | BDI        | @ndcn1032     | 0000-0001-6574-4789 |


## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Admin
- hack-day:
    - 1 vegetarian + lactose intolerant

## 2. Poster for OxFOS event
- Kirri to send off application form today
- Tara has started on a template


## 3. Project Updates

### Data sharing
- meeting on 18 Feb: with Tom Nichols, Marieke Martens, Ludo Griffanti, Yifan, Bernd
- waiting to hear back from information governance (IG) team at NDPH 

### Kirri's meeting with Jason
- ORA collections (e.g. climate, AI) -- want to create a neuroscience one
- good news stories: examples of where open access resources were useful
    - blog-post style (ca. 1000 words) -- e.g. Tara's project
- researcher profiles on symplectic: try to improve profiles
    - Kirri will meet and then develop a template / guide at WIN
- short-form hosting servics: 
    - could the ambassador programmes have a journal? -- one annual issue
    - e.g. annual reports, reflections from past ambassadors, practical tips (on how we do things)
    - goal: to capture researchers' activities across years (record of achievements for centre in general)
    - Bodleian has a pilot with another group at the moment
    - for new pilot: need a (semi-permanent) editor 
    - audience: new ambassadors, CV, centre output (e.g. at ambs event), publicly accessable 
    
### Training sessions for senior researchers
- Tara to come up with a structure for the sessions for hack day
- Search how to approach and 'teach' senior researchers in bite size sessions rather than long ones
- Work on how to advertise and what topic to start with


## 4. Gitlab
- Séamus: issue with trailing backslashes on open community pages
- useful links:
    - your projects: https://git.fmrib.ox.ac.uk/ 
    - open WIN community: https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community
    - ambassadors only repo: https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors



<br>

---

## ==Actions==
- [ ] Bernd: organise hack day (room, catering, Stuart)
- [ ] Kirri to meet with PI to get account sorted
- [ ] Kirri to send off OxFOS form today


## Upcoming
- [ ] Next meeting: 06 Mar
- [x] hack day: 06 Mar, 10am-3pm


<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

