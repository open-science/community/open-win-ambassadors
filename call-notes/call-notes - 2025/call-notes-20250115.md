:::info
- Date/time: Tuesday 15 Jan 2025, 09:30-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/BkacBnfw1g/edit)
- Last meeting notes: [08 Jan 2025](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HkBV9tqIye/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 15 Jan 2025

## Agenda
1. Admin
2. Markdown
3. Open Science topics


## Participants

| Name                | pronouns | department | WIN gitlab ID | ORCID |
| ------------------- | -------- | ---------- | ------------- | ----- |
| Kirralise Hansford  | she/her  | Women's and Reproductive Health | TBC | 0000-0002-5738-6843 |
| Tara Ghafari        | she/her  |  TBC       | TBC           | 0000-0002-5178-8702 |
| Vivian (Yifan) Yang | she/her  | Experimental Psychology    | @grx569  | 0000-0003-0211-1313 |
| Séamus O'Sullivan   | he/him   | Psychiatry | @fhv480       | n/a |
| Bernd Taschler      | he/him   | BDI        | @ndcn1032     | 0000-0001-6574-4789 |


## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Admin
- gitlab accounts
- shout out to Séamus for single-handedly updating the backbone of our community pages!
- ... ?


## 2. Markdown

### Basics
- [cheat sheet](https://www.markdownguide.org/cheat-sheet/)

### Text editors
- sublime text
- (neo)vim
- VS code
- (emacs)
- ...


### command line / terminal
![cheat sheet](https://hackmd.io/_uploads/H1U76VND1l.png)



## 3. Open Science topics
- Discussion: [life cycle of a research project](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/getting-started/)
- [preregistration](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/preregistration/)
    - Kirralise's experience with registered report and PCIRR --> maybe organise a WIN Wed, add experience report to community pages
- [licensing](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/repo-license/)
- [open access publishing](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/oa-publishing/) --> [WIN OA policy](https://www.win.ox.ac.uk/research/publications/open-access-policy)



<br>

---

## ==Actions==

- [ ] WIN IT accounts
- [ ] ...


## Upcoming

- Ambassadors day: 29 Jan 2025


<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

