:::info
- Date/time: Tuesday 08 Jan 2025, 09:30-10:30 (BST)
- Location: MS Teams
- These notes: [hackmd](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/HkBV9tqIye/edit)
- Last meeting notes: [10 Dec 2024](https://hackmd.io/@qEqY6y1AT1ORGCqRMRspnQ/SyTmsteNJx/edit)
- highlighted: ==action items==
:::

# Open WIN Ambassadors meeting - 08 Jan 2025

## Agenda
1. Admin
2. Reboot camp 1
3. Plans for 2025


## Participants

| Name                | pronouns | department | WIN gitlab ID | ORCID |
| ------------------- | -------- | ---------- | ------------- | ----- |
| Kirralise Hansford  | she/her  | Women's and Reproductive Health | TBC | 0000-0002-5738-6843 |
| Tara Ghafari        | she/her  |  TBC       | TBC           | 0000-0002-5178-8702 |
| Yifan (Vivian) Yang | she/her  | Experimental Psychology    | @grx569  | 0000-0003-0211-1313 |
| Séamus O'Sullivan   | he/him   | Psychiatry | @fhv480        | n/a |
| Anna Guttesen       | she/her  | FMRIB      | @nnm329       | 0000-0003-0284-1578 |
| Bernd Taschler      | he/him   | BDI        | @ndcn1032     | 0000-0001-6574-4789 |


## Participation guidelines
- We value the participation of every member of our community and want to ensure that each of us has an enjoyable and fulfilling experience. Please show respect and courtesy to other community members at all times.
- We are dedicated to a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, neurodiversity, physical appearance, body size, race, age, religion, politics or technology choices. We do not tolerate harassment by and/or of members of our community in any form.
- We welcome, support and respect each other as whole people.
- We fall under the formal policy and reporting guidelines of the University of Oxford Bullying and Harassment Policy and we expect everyone to be a responsible bystander.

----

## 1. Happy New Year!
![image](https://hackmd.io/_uploads/ryXcHZjUkx.png)

### Admin
- 2024 <--> 2025 ambassadors introductions 
- quick messaging: [our new MS Teams channel](https://teams.microsoft.com/l/channel/19%3A5965158188574f5e89263dcafc80201d%40thread.tacv2/Open%20WIN%20ambassadors?groupId=47188afe-2e27-4f47-bbf2-2ec8840e9bfb&tenantId=cc95de1b-97f5-4f93-b4ba-fe68b852cf91&ngc=true)
- ambassadors [profile page](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/ambassadors/meet_ambassadors/)
    - [x] 2025 ambs: photos and blurbs
    - announcement of new ambassadors in next Monday's WIN message
- [ ] WIN computer accounts?
    - [ ] Tara waiting for SSO (probably Feb)
    - [ ] Kirralise waiting for PI approval
    - [ ] Yifan waiting for PI/funding confirmation for account
- ambassadors day (29 Jan):
    - [ ] lightning talks (?): Anna, Lara, Ying-Qiu, ~~Lilian~~, Juju, Yingshi


## 2. Reboot camp 1
- *when*: Thu, 23 Jan, 1000-1300
- *where*: FMRIB Annexe, Cowey rooms
- *lead*: ==Juju==
- *support*: ??
- *what*: Intro to open science


## 3. Plans for 2025

### Info / Training for new ambassadors
- editors (markdown, sublime text or similar)
- version control (git / gitlab)
- community pages
- MR protocols database
- OS topics (data/code sharing, licencing, open access publishing, etc. )


### EEG database
- *lead*: ==Anna, Lilian, Seamus==
- xnat integration
- guides and examples
- working with IT, Seb Rieger, Sven Braeutigam


### MR protocols database
- *lead*: ==Seamus==
- maintenance 
- minor updates and improvements (e.g. anonymisation)
- integration of preclinical scanner (Bruker system)
- ideas to increase usage


### Open Tasks
- *lead*: ==Seamus==
- example templates
- guidelines
- MEG (*Tara*)


### WIN Monday messages
- short, engaging, interesting, funny?
- info, pointers, news, reminders, help, etc. 
- can be sent in bulk to admin@win
- *lead 2024*: Lara
- *lead 2025*: ==Kirralise==


### Data sharing
- *lead*: ==Yifan==
- Data Protection Impact Assessment (DPIA) requirements
- General Data Protection Regulation (GDPR) 
- ethics requirements
- licencing
- need guidelines for whole process
- example projects:
    - with Marieke Martens (psychology)
    - with Ludo Griffanti (brain health clinic)


### Outreach and contacts to wider university
- *lead*: ==Kirralise==
- Bodleian library
- research services
- [Reproducible Research Oxord](https://rr.ox.ac.uk/) (RROx)
- NIHR training sessions (Tara to F/U)


### WIN Wednesday seminar series 
- "success stories"
- invited speakers
- OS specific topics
- short talks
- ...


### Teaching and training at WIN
- *lead*: ==Tara==
- git / gitlab
- OS specific topics
- coding with copilot (?)
- graduate course: Reboot camp 2
    - one day during week starting 28 April


### Community pages
- updating and continuous development of information (see e.g. gitlab issues)
- new guides / topics
- ambassadors repo for any resources, presentations, work in progress, etc. 
- new guide on MEG to BIDS conversion (*Tara*)
- 

### Community support
- open@win.ox.ac.uk email
- highlighting open research at WIN (e.g. a poster repository, etc.)


### WIN away day (?)
- 24 April


### Hack days
- in-person meet-up once per term
- next one: Feb / March ?

----

### Overall Goals 2025 (suggestions)
- learn (more) about open science best practices
- launch EEG database
- create a guide for how to share imaging data with DPIA (get an example project across all necessary steps) 
- resolve all legacy (opened before 2025) git issues 
- deliver training at reboot camps 1 & 2
- deliver git training for wider WIN community
- organise 3 WIN Wed seminars
- engage with WIN community through WIN Monday messages (weekly), open@win email, in-person chats, etc. 
- develop deeper links with relevant groups/people across wider university
- work together during 3 hack days
- ... ?


<br>

---

## ==Actions==

- [ ] get WIN computer accounts sorted


## Upcoming

- [x] Next meeting: 15 Jan
- [ ] ambassadors day: 29 Jan


<br>

---

## Feedback on this session
*(Anything you found particularly interesting; something that worked well or not so well; open questions; suggestions, comments, etc.)*

