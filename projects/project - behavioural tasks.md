# Project Title

**Project Lead**: Juju

## Resources
*Any relevant and useful stuff, links, etc. for reference.*

- [Open WIN community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/)
- for Psychtoolbox tutorials: Peter Scarfe - Scarfe Lab (see https://peterscarfe.com/ptbtutorials.html)
- for scripts that use staircase procedures: see the owrk of Alex S Baldwin - McGill Vision Research

<br>

## Aims/Objectives 
*What do we want to achieve/deliver with this project?*

With this project, we aim to make psychophysical experiences available. When young researchers start writing behavioural experiments, there are many online sources to get started. On the other hand, it is very difficult to find complete scripts; or scripts that have been used in studies. 
Recent scientific papers often make available the analysis scripts and data used, but few actually publish the experiments' scripts used to obtain the raw data. This, by definition, does not help to reproduce the results. 
The aim of this project is to make available scripts ready for use by researchers. They are not intended to be perfect ( or fully optimised), but they will be commented on sufficiently to enable a rapid understanding of the code, which, in the end, may help researchers to adapt the script to their needs. 


### Main Milestones 
*What are the main components of the project?*

- creating a battery of visual behavioural tests on a Matlab/Psychtoolbox setting 
    This setting is still used by a lot of researchers, especially when external devices are used (eye tracker, MRI, EEG...)
- make the scripts more flexible
    The actual scripts answer to my needs (Juju). Other researchers may like a version of the program that is not dependant of my constraints.
- comment the scripts for a better understanding 
    Most of the functions/variables must be explained. 
- add the possibility to do eye tracking while using the scripts
    Related to my needs, but still useful for others. 
- publish the scripts
    On the lab's Gitlab
- modify the scripts if needed/if feedback or if updates (Matlab, Psychtoolbox, etc...)

### Timeline
*What is a (currently) realistic timeframe?*

- creating a battery of visual behavioural tests: 02 / 2023
- make the scripts more flexible: 07 / 2023 
- add the possibility to do eye tracking while using the scripts: 05 / 2023
- comment the scripts for a better understanding: 06 / 2023
- publish the scripts: 07 / 2023
- modify the scripts if needed/if feedback or if updates: no deadline
- Expected project completion: 08 / 2023

<br>

## TODO

### Open Questions / Issues
*Are there any issues, questions, etc.?*

- ..
- ..

### Next Tasks / Actions
*What are the next concrete steps (tasks that can be done in 1h or less)?*

- [ ] ..
- [ ] ..
