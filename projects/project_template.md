# Project Title

**Project Lead**: Sam & Susie Sample 

## Resources
*Any relevant and useful stuff, links, etc. for reference.*

- [Open WIN community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/)
- ...

<br>

## Aims/Objectives 
*What do we want to achieve/deliver with this project?*

- ..
- ..

### Main Milestones 
*What are the main components of the project?*

- ..
- ..

### Timeline
*What is a (currently) realistic timeframe?*

- Complete next milestone: xx / 2023
- Expected project completion: xx / 2023

<br>

## TODO

### Open Questions / Issues
*Are there any issues, questions, etc.?*

- ..
- ..

### Next Tasks / Actions
*What are the next concrete steps (tasks that can be done in 1h or less)?*

- [ ] ..
- [ ] ..
