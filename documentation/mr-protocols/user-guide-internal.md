# User Guide: WIN MR Protocols Database

> ## Purpose
>This document is designed to provide detailed guidance on usage of the WIN MR Protocols database for internal (WIN) users. Guidance text will be drafted here and published on the Open WIN [community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tools/acquisition/) when finalised.

> ### Scope
>This guidance is designed to cover all [internal use cases](../use-cases.md) of the MR Protocols database. If you would like to search for an MR protocol other WIN users have uploaded, please have a look at the [external user guide](../user-guide-external.md) which explains how to search for and download a protocol.

> Bug reports or feature requests identified in creating this material should be communicated to the developers by adding to [this issue on the protocols database repo](https://git.fmrib.ox.ac.uk/acquisition/win-protocols-db/-/issues/43).

> ### Intended Audience
>The guidance written here is intended to be reviewed by WIN Operations, Radiographers, IT, and feedback invited from the commnunity, before being published on the community pages. The guidance written is aimed at WIN researchers (of all levels) who are users of the WIN MRI scanning facilities across any WIN site.

## Contents
- [Why should I use the MR protocols database?](#why-should-i-use-the-mr-protocols-database])
- [Why should I make my protocol public?](#why-should-i-make-my-protocol-public)
- [How do I use the MR protocols database?](#how-do-i-use-the-mr-protocols-Database)
  - [Adding a new protocol entry to the database](#adding-a-new-protocol-entry-to-the-database)
  - [Issuing a doi and license](#issuing-a-doi-and-license)
  - [Editing your protocol entry](#editing-your-protocol-entry)
  - [Tracking engagement](#tracking-engagement)
- [Suggest changes to this guidance](#suggest-changes-to-this-guidance)


## Why should I use the MR protocols database?
The MR protocol database has been developed to share MR protocols internally across WIN as well as externally. The database provides access to standard protocols and to the latest experimental protocols uploaded by WIN members. The database facilitates sharing your MR sequence with your and other research groups and helps you to keep track of updates via version control. Each deposited protocol contains all information necessary to reproduce that sequence on an appropriately licensed scanner, allowing yourself and others to have a permanent and full record of your data acquisition methodology.


## Why should I make my protocol public?
1. You (and the radiographers) receive the credit for this part of the work (you can include citable DOIs)
2. Makes it easier to share this information with colleagues and potential collaborators
3. Keeps a reproducible record for your lab of all details and decisions made (version control)
4. You contribute to open and reproducible research since other researchers might reuse your MR protocol. This will speed up translation of your research findings and will ultimately increase the impact of your research.

## How do I use the MR protocols database?

### Adding a new protocol entry to the database

#### 1. Prepare the required files

##### 1.1 Ask your radiographer for your protocol pdf and scanning procedure file.
Your scanning procedure file (.doc) and scanner protocol (.pdf) will be given to by your radiographer at the start of your project. You will be provided with a new version if there are changes in the protocol. You can request copies of these files by emailing radiographers@win.ox.ac.uk with your calpendo project number.

##### 1.2 Request permission from the radiographer author of the scanning procedure to add them as an author on the public doi record of your protocol.
The radiographer author of your scanning procedure is listed at the top of the file. Contact this radiographer and ask their permission to be listed as a contributor to this protocol on the public doi record. Request the radiographers ORCID ID for inclusion.

##### 1.3 Redact confidential information from the scanning procedure and convert it to a pdf.
Any names should be redacted from the scanning procedure unless permission to be listed has been provided by those individuals. Review the complete scanning procedure document (including instructions such as "check voxel location with <NAME>") and redact all identifiable information which you do not have permission to share. These details can be replaced with the word "REDACTED" or covered with a black rectangle.

Review all images and ensure that identifiable information such as participant IDs are redacted (cover with a black rectangle). Ensure identifiable facial features (in sagital views of slice pack positioning) are redacted (cover with a black rectangle).

Currently, only the owner of the project is able to edit the uploaded protocol. If you would like to suggest changes to someone else's protocol you can do so by emailing the admin team who can then contact the project's owner.

You might want to add a statement such as "available for re-use, but I'm unable to assist with implementation" in the usage notes to clarify the extent of assistance you are able to provide.

#### 2. Upload

##### 2.1 Log in to the database.

Only WIN members (as determined by SSO authentication) are able to deposit protocols in the database.

![gif-log in](./figures_internal/gif_login.gif)

To log in:
1. Go to [https://open.win.ox.ac.uk/protocols/](https://open.win.ox.ac.uk/protocols/)
2. Click the "Login" icon on the top right of the screen
3. Click the "Login via Shibboleth" option and enter your Oxford SSO details when requested.



##### 2.2 Add a new protocol entry

Protocols are saved to individual user profiles.

![gif - add new protocol](./figures_internal/gif_add-protocol.gif)

1.  Once you are logged in, you will see the "**Add Protocol**" icon.
2. Click on "**Add Protocol**", and you will be taken to the "**Add new protocol**"" page.
3.  Click the "**Browse**" button to open a file explorer.
4.  Navigate in the file explorer to the scanner protocol (.pdf) you want to upload. Select "open" in your file explorer.
5. Click on the "**Add new protocol**" button to upload the selected pdf to the database.



##### 2.3 File type error
If the uploaded file is recognised as a sequence pdf, you should see the **Register Protocol** page. If the file is not in the right format, you will see an error. Please check the file you have selected is a pdf of your MR protocol.

#### 3. Top level protocol information
You can enter details about your protocol which are relevant to the study as a whole, or all sequences. These details are entered at the top of your protocol. These details will help future users understand your decisions or implement your protocol.

![gif - top details](./figures_internal/gif_top-details.gif)

##### 3.1 Project, hardware and species
Some fields of this section are identified by the database from your pdf. Others have to be selected by you. Complete the following selections by selecting the appropriate item from the dropdown menus.
1. Project (select from Calpendo registered projects)
2. Scanner
3. Species
4. Post mortem (tick if appropriate)

##### 3.2 Keywords
Keywords can be used by users to find your protocol. Note that when users are searching for a protocol, their search query will be matched to keywords only, not text in the body of the entry.

Start typing keywords to select from a built in list. Consider adding keywords relating to:
1. The sequence type (e.g., "T1 structural")
2. Regions of interest
3. The participant population or clinical condition
4. Cognitive or behavioural function
5. Task or stimulus properties


##### 3.3 Description
This section should identify and highlight important features of the protocol. For example the study design, methods, and procedure. The Radiographers procedure abstract is sufficient.


##### 3.4 Usage guidance
This section should provide high-level instructions for use of this protocol. Where possible, we recommend including the following:
1. **Contact details** for anyone looking to discuss re-use of the project. Note this information will be publicly available if your protocol is made public. Only include contact details where explicit permission has been granted.
2. **How to cite** this entry, including any references you would like to be cited in addition to this entry (see [Issuing a doi and licence](#issuing-a-doi-and-license) below)
3. **Your terms for reuse** (see [Issuing a doi and licence](#issuing-a-doi-and-license) below), including any specific terms around authorship before others reuse this protocol. For example, "We request that future users of this protocol contact the authors of this entry to discuss collaboration and authorship agreements relating to reuse." This may be particularly relevant for experimental protocols or sequences.

You may also like to include some practical guidance, such as the total length of the scanning session, staffing levels, or any peripheral equipment. You may refer the reader to the radiographers procedure file if this is to be attached (see [4. Attachments](#4-attachments)).

You should also highlight non-standard software or sequence requirements, such as custom pulse sequences.


#### 4. Attachments
Browse to and add any attachments to support the implementation of your protocol. This should ideally include the (redacted) radiographers procedure. You may additionally chose to attach analysis of pilot data, for example.

*Note that there is a file size limit of 15MB for each individual file attached*

#### 5. Sequence level protocol information
You can add details specific to individual sequences. These details may make it easier for users of this protocol to understand the outputs if the protocol is run, information about why these sequences where chosen or refer to decisions made during pilotting. You may reference back to earlier versions of the protocol entry to indicate changes where appropriate.

Each of the sections below should be completed for each sequence.

![gif - sequence details](./figures_internal/gif_sequence-details.gif)

##### 5.1 Sequence name
This is identified from the pdf. You can update it or add any additional context where necessary.

##### 5.1 Sequence keywords
1. Keywords including for example modality, ROIs, clinical conditions, study population, study type.

##### 5.2 Sequence description
The sequence description provides key features that might help someone choose whether it meets their needs. For example, “A standard T1-weighted structural scan, of 1mm isotropic resolution, suitable for registration to a standard template in an FMRI protocol.".

It may additionally be useful to disambiguate any abbreviations in the name of the sequence, for example "bold_mbep2d_MB4P2_task_faces" could be described as "multiband 2D echoplanar sequence; multiband acceleration factor 4, PAT (GRAPPA) factor 2; for faces task.".

For fieldmap sequences, it may be useful to say which BOLD sequence they will be used to correct.

Where a specific task is mentioned, you could link to where that task is shared, or published analysis of any data using that task.

#### 5. Save
Once all required fields have been completed, you will be able to save the entry to the protocols database. Press "Save" to progress to return to the list of your protocols


#### 6. Set visibility
By default, your entries will be set to "Internal (University Only)" visible when you add them to the database. This means users will be required to log into the the protocols database with an SSO <mark>confirm WIN SSO?</mark> in order to review or download your entry.

After they have been entered onto the database you can make individual protocols publicly visible ("Open Access") and identify them as "WIN Recommended" protocols as appropriate.

"Open Access Actions"
If you choose to make a protocol public ("open access") ensure you have added a license note to your entry, reserved a doi for the protocol using zenodo, and added the doi to the usage guidance (see [Issuing a doi and licence](#issuing-a-doi-and-license) below).

#### 7. Citation <mark>(Reference)</mark> List
This field features prominently at the top of the entry when it is accessed by users, so it may be appropriate to repeat any important citation information which is listed in the usage guidance.

Add any references which are relevant to this protocol, for example the paper or pre-print which it accompanies. You can also use this space to draft the author list and track changes in contribution.

We recommend that you consider using the [CRediT](https://casrai.org/credit/) system for identifying individual author contributions. You may wish to order authors alphabetically.

![gif - references](./figures_internal/gif_references.gif)

### Issuing a doi and license
We suggest you use the free tool Zenodo for creating a doi for your protocol. This will allow others to cite your protocol independently from other work in the study, and add contributors who may not be named elsewhere.

#### Who to include as an author

This is an opportunity to provide attribution to all individuals who contributed to this project up to the point at which the scanning protocol was finalised and/or published. This may include individuals who contributed to each of the stages below:
- Gaining ethical approval
- Registry as a clinical trials or pre-registration
- Sequence development or pilotting
- Analysis of pilotted data
- Authoring of the Radiographer's procedure

##### ORCID
Where possible, we promote the use of ORCID to consistently identify and attribute authors. Request that your contributors provide their ORCID for inclusion. Alternatively you can look them up on the [ORCID database](https://orcid.org) but make certain that you have found the correct individual.

##### CRediT
You may wish to consider assigning authorship using the [Contributor Roles Taxonomy (CRediT) notation](https://casrai.org/credit/), which is now formally recognised by major publishing houses. Using this standard, the contributions of each individual are clearly stated against pre-defined contributor roles. This taxonomy is often paired with alphabetical ordering of authors, rather than prioritising "first author" and "last author" positions.  

##### Tenzing
You can write a CRediT statement by hand, or you can use a tool to curate the required information. The [Tenzing app](https://rollercoaster.shinyapps.io/tenzing/) can use a csv table of author information to generate CRediT statements in multiple different forms.

You can collect the author information (for example ORCID, affiliations, funding acknowledgements) using the google table linked via Tenzing (see [How to use tenzing](https://rollercoaster.shinyapps.io/tenzing/)). Alternatively you are welcome to take a copy of this [MS Form: Authoring information for CRediT statement (Open WIN)](https://forms.office.com/Pages/ShareFormPage.aspx?id=G96VzPWXk0-0uv5ouFLPkYMD50Te0q5HobQjqRFNJmpUM0NHR01CTkxZV0dTMUxLR0hQOTJGTURUTC4u&sharetoken=2cQ8XQ8eqkhqEhTRQT51) to collate your author information. Send the form to each of your intended authors and use the information collected to generate the csv table required by tenzing.


#### Using Zenodo

The below basic process outlines how to upload a document to zenodo to generate a doi. We have also provided boilerplate text suggestions for use where relevant.

Note once you publish your protocol with zenodo, it cannot be deleted. If you are doing this for the first time, or are otherwise uncertain of the process, Zenodo provides a practice sandbox website for testing. This can be found at [sandbox.zenodo.org](https://sandbox.zenodo.org/). The website interface is identical to the ‘real’ [zenodo.org](https://zenodo.org/), but DOIs issued on the sandbox aren’t real. Note that you 1) have to make a separate account on the sandbox site; and 2) that there is no way of converting a sandbox listing to a real one, you will have to replicate the steps you took on [zenodo.org](https://zenodo.org/).

1. Create a zenodo account using your ORCID ID or log into an existing zenodo account.
2. Go to "Upload" and select "new upload".
3. Set the file type to "other" and **tick the "reserve doi" box**
4. Enter a title. We suggest you use: `WIN MR Protocol: [your protocol name]`
5. Add authors as described above, including their ORICD.
6. Description: Suggested text below
```
Protocol for the acquisition of MRI data for the study "[your protocol name]".

Conducted at the Wellcome Centre for Integrative Neuroimaging (WIN), University of Oxford.

Please see the entry of this this protocol in the WIN MR Protocols Database to ensure this is the latest version: [your stable url, provided by the protocols database]

Author list and CRediT roles: [your author list]

Additional papers to be cited: [publications relating to this protocol, including preprints]

Acknowledgements: [acknowledgements, for example funders]
```
7. Copy the reserved doi, title and authors into your Usage Guidance section on the protocol database entry. It may be helpful to look at existing zenodo entries to see how this will be formatted, for example the zenodo entry for [2018_114 Seven Day Prucalopride](https://zenodo.org/record/6107725#.YhUSWy2cZBw).
8. Download your protocol entry from the WIN database in pdf form. If your entry contained attachments, zip it into a single directory.
9. In the files section at the top, click "choose files" to locate your protocols database entry (pdf or zipped directory). Once located, click "start upload".
10. Fill in any additional language, keyword or notes information as you wish.
11. Set your access requirements and choose a license as appropriate. We suggest a [CC-BY-4.0 license](https://creativecommons.org/licenses/by/4.0/)

### Editing your protocol entry
If you are using the protocols datbase to track changes made to your protocol while pilotting, you can amend the entry and create different version which can be reviewed at any point.

![gif - update](./figures_internal/gif_update.gif)

1. Go to "my protocols" and find the entry you would like to update.
2. Select whether you are adding a new sequence pdf (`Update with new protocol file`) or making a change to other parts of the entry only (`Update keeping existing protocol file`)
3. Identify whether you are making a minor amendment to descriptive text, incrementing the "edit" number only, or a more substantial update, incrementing the version number.
4. Make the required changes.
5. We recommend adding a "Change Log" section to the `Description` field, to record what changes were made and why.
6. Save your edits using the `save` button at the bottom of the page.
7. You can review your edits by clicking on `full history` on any protocol entry.

### Tracking engagement
You can track how many times your protocol has been viewed and downloaded from the `Engagement Statistics` panel. Views and downloads are separated into internal to Oxford (where the user has logged in with an SSO) or external. When reporting on the impact of your entry, remember to also include the zenodo download and view statistics, if you used zenodo to create a doi.

### Transferring ownership
If you are unable to log in with an SSO (your Oxford account has expired), you will need to transfer ownership of your protocol to someone else (potentially the Principal Investigator on the project). To arrange this, please contact admin@win.ox.ac.uk and request that the entry is transferred.

## Suggest changes to this guidance
If you would like to suggest changes to this guidance, please submit an [issue on the Open WIN Community gitlab repository](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues). Please include a link to the page you are suggesting edits to.

# <mark>Notes for us when writing</mark>
Things to add:
- [x] Note that the uploader has should enter some contact information in the "usage instructions", for when people find this protocol and want to re-use it.
- [x] Currently only owners can edit. If you see something which you think should be changed on someone else's protocol, you need to email admin and they will contact the user (we are not automatically revealing uploader contact info for data privacy reasons)
- [x] How to create a doi (zenodo)
- [x] Users may want to add something like "available for re-use, but I'm unable to assist with implementation" to their usage notes.
- [x] Make sure we cover versioning.
- [x] Required to login via SSO (Shibboleth)
- [x] What should be included in the description for each sequence?
- [x] How to cite
- [x] Give info on how to track "Engagement Statistics" report and why it is important!
- [x] Give link to how to feedback on this documentation (gitlab issues)
- [x] Where to include notes when pilotting to indicate changes, including ref to previous version of the entry.
- [x] Transferring ownership if/when you leave; requesting ownership when someone has left; contacting admin to find owner.
- [x] File size limit for attachments - 15MB
