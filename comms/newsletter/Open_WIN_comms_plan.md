
## The Idea
- Goals:
    - Increase WIN's and the publics exposure to, and proficiency in, open-science practices
    - Signpost useful open-science resources for researchers
    - Promote excellence in open-science at @WIN
- To achieve these goals we want to broad cast information about open science @WIN through two channels
    - **win2win**: internal win communication
        - eg, the Monday message, WIN Wednesday
    - **win2world**: more public communications
        - eg, Twitter, Mastadon, openWIN mailing list

## Target Audiences
- We want to target different audiences with the win2win and win2world comms channels
    - win2win: WIN staff & students
        - specific audiences within WIN: students, ECRs, PIs, those new to open-science and experienced open-sciencers
    - win2world: public (aknowledging that we will also reach lots of people at WIN)

## win2win Examples
### Monday Message

1. Open WIN Community pages - DRAFT. <br />
**Target Audience**: WIN - ECR <br />
The Open WIN Community pages  are a hub for information on how to employ open research practices at WIN, including info on how to share MR protocols, raw and processed data, analysis pipelines, code, and much more. Find out how to share your research outputs effectively and responsibly, for improved impact, access and collaboration: https://www.win.ox.ac.uk/open. Contact the Open WIN Ambassadors for more information: open@win.ox.ac.uk

~~2. New Title - DRAFT. <br />
**Target Audience**: Win - x <br />
Find out more about the Open WIN Community, what we’re doing, and how you can be a part of a larger effort to make neuroimaging open, transparent and reproducible.~~

~~3. New Title - DRAFT. <br />
**Target Audience**: Win - x <br />
Watch Laurence Hunt as he argues that data sharing may carry tangible benefits to your own research that can outweigh any potential associated costs.~~

4. GitLab training resources - DRAFT.  <br />
**Target Audience**: WIN - ECR <br />
Not sure what all the fuss is about with git? Want to get your project onto GitLab but not sure where to start? The Open WIN Community pages contains a range of GitLab [tutorials](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/) that will get you started and can be referred back to by more expereinced users (e.g. how to turn your repository into a website using [GitLab Pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/4-4-create-gitlab-pages-site/)). 

5. Data Sharing Guideance - DRAFT. <br />
**Target Audience**: WIN - ECR/PI <br />
Research data should be shared to maximise the scientific potential of this useful resource, but, you need to think carefully about how you will protect your participants and your own investment. Take a look at the [Open WIN Community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/) for guides on why, how and when you should share your data. 

6. Open Science Training - DRAFT. <br />
**Target Audience**: WIN - ECR <br />
Looking for an open science event to connect with like-minded researchers, hear about new initiatives or discuss ongoing projects? The Centre for Open Science has a list of upcoming in-person conferences, online events and seminars that may be of interest. Take a look at [Open Research Calendar](https://openresearchcalendar.org) to subscribe to event and training opportunity listing via email. 

### WIN Wednesday Seminar series: "Open Science Success Stories"
- 15-20 min talk
- Orgnaised by ambassadors (one each per year).
- Tone = Motivational. "Look how easy it was an how it was beneficial"
- Speakers external or internal


## win2world Example
**Paper highlight for Twitter/Mastadon**:
1. What can combining open datasets across species tell us about brain-wide serotonin dynamics? <br />
https://doi.org/10.1038/s41593-022-01213-3 (1/n)

2. @neuropg, @heidijoberg et al., combined behavioral, transcriptomic, and opto/resting-state fMRI analyses to show that “brain-wide distributions of different serotonin receptor types may underpin behaviorally distinct modes of serotonin regulation”. (2/n)

3. And impressively, they made these findings with fully open-source tools and data from: the Allen Institute, the Human Connectome Project, Grandjean et al. (2019), and the FMRIB Software Library. Let’s take a look at these tools and datasets (3/n)

4. The Allen institute has… (4/n) <br />
	https://human.brain-map.org/microarray/search <br />
    https://mouse.brain-map.org/agea

5. The human connectome project has…(5/n) <br />
	https://db.humanconnectome.org/

6. Openneuro.org hosts hundreds of BIDS-compliant MRI, PET, MEG, EEG, and iEEG datasets. Including… (6/n) <br />
	https://openneuro.org/datasets/ds001541/versions/1.1.3
