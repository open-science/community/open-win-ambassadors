# Drafts and Ideas

*This document includes any ideas, suggestions, drafts and thoughts on a (potential) Open Science section in the WIN Monday newsletter.*

## First Drafts 

**1. Open Science Bite of the week** 🍪 - *DRAFT*. \
Find out how to share your research outputs effectively and responsibly, for improved impact, access and collaboration: [https://www.win.ox.ac.uk/open](https://www.win.ox.ac.uk/open) 
The Open WIN Community pages  are a hub for information on how to employ open research practices at WIN, including info on how to share MR protocols, raw and processed data, analysis pipelines, code, and much more. 

**2. Open Science Bite of the week** 🍪 - *DRAFT*. \
Find out more about the [Open WIN Community](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/), what we’re doing, and how you can be a part of a larger effort to make neuroimaging open, transparent and reproducible.

**3. Open Science Bite of the week** 🍪 - *DRAFT*. \
Watch [Laurence Hunt](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/open-research-benefits/) as he argues that data sharing may carry tangible benefits to your own research that can outweigh any potential associated costs.

**4. Open Science Bite of the week** 🍪 - *DRAFT*. \
Not sure what all the fuss is about with `git`? Want to get your project onto `GitLab` but not sure where to start?
The [Open WIN Community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/) offer lots of useful information, including a range of [GitLab tutorials](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/gitlab-tutorials/) that will get you started (and have you covered when you need to remind yourself of how to issue a [merge request](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/3-2-collaborating-fork-their-repo/)).

**5. Open Science Bite of the week** 🍪 - *DRAFT*. \
Not every dataset can or should be shared with everybody. But if you can, there are many reasons [why you should share your data](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/why/)?
And if you are at a loss as to what online repository or platform to use, we have some info on the [Open WIN Community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/how/) or you can check out a recent blog post by Dmytro Kryvokhyzha [comparing *Dryad, Zenodo, FigShare, Open Science Framework, and Mendeley*](https://evodify.com/free-research-repository/).

**6. Open Science Bite of the week** 🍪 - *DRAFT*. \
Looking for an open science event to connect with like-minded researchers, hear about new initiatives or discuss ongoing projects? The [Centre for Open Science](https://www.cos.io/events) has a list of upcoming in-person conferences, online events and seminars that may be of interest. 

...

---

## Comments 

*Any suggestions / additions / corrections / etc. welcome!*

- Pros/Cons of the cookie emoji in the header?
    - CGVP: I love an emoji, but I'm not sure it will be accepted in the Monday Message as it is 'text' only...
- Thoughts on length per message?
- CGVP: I was thinking about why the "intranest highlight" part of th e monday message is so unappealing (for me at least!). Part of it is becasue I have to read the whole thing to work out if it's relevant/interesting/useful. For the rest of the message I decide whether or not to read it based on the title, so the "intranet highlight" is alsways making me to extra labour and there is a decent chance it will be a waste of my time! Instead of calling this segment "Open Science Bite", how about we just give it a title which is relevant to the topic? This also means we might catch people who wouldn't normally engage with "open science". 


## Monday Messages Ideas (Peter)

*DRAFT*
**Questions about data sharing, method sharing or anything open science?**
- Your open WIN ambassadors are here to help. Reach out to the open WIN [email](mailto:open@win.ox.ac.uk) with any questions! For more information about us, see our Open WIN community pages [here](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/)

*DRAFT*
**What tools can I use share my data with callaborators, reviewers or the world?**
- [Open Science Foundation (OSF)](https://osf.io/): has built-in version control, customisable access links and integration with OneDrive, Dropbox and Google Drive to share large data files. Note that projects that require access to be managed on a large scale can be tricky to coordinate in the current version of OSF. 
- [Zenodo](https://zenodo.org/): lets you share all types of research outputs including, datasets, software, videos and reports with an assigned DOI (digital object identifier). You can tightly control the licencing of your data and who has access to it too!
- [Neurovault](https://neurovault.org/): is an easy way to share statistical brain maps (any brain data stored in a 3D NIFTI format). This can be a great place to share group/participant level contrasts to increase the transparency to your publications. But note, data added to NeuroVault can be reused for any purpose, including commercial ones, without mandatory attribution for reuse​. \

For more details and data-sharing options see the [OPEN WIN community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/how/) or email us at [open@win.ox.ac.uk](mailto:open@win.ox.ac.uk).

*DRAFT* 
**Looking to get started with Git and GitLab?** \
Check out the OPEN WIN community [guide](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/), which includes a range of tutorials from using the command line, setting up your GitLab account, understanding basic Git commands, to creating a repository and making it citable.\

If you have any questions don't estiate to contact you OPEN WIN ambassadors at [open@win.ox.ac.uk](mailto:open@win.ox.ac.uk).


*DRAFT*
**Want help promoting your research?**
 If you are planning on sharing code or data from a recent project, we would like to hear about it! The Open WIN ambassadors can help promote any open science you do, just contact us at [open@win.ox.ac.uk](mailto:open@win.ox.ac.uk).


*DRAFT*
**Wondering about best practises for data science?**
[The Turing Way handbook](https://the-turing-way.netlify.app/index.html) contains guides on how to write reproducible and sharable code, how to use version control for your code and data, how to licence you work and much much more. Its a great supplement to the information already available on the [Open WIN commmunity pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/how/)


*DRAFT*
**What is Conda? and how can it help you make your computational environments reproducible?**
[Conda](https://docs.conda.io/en/latest/) is a an open source package management system and environment management system that lets you easily creat, save, load and wwitch between computational environments on your local computer. Conda also lets you package your Python environments into .YAML files to ensure that other users can easily reproduce you're environments to use you're shared data and code! For more info on how Conda works and what you can do with it [here](https://the-turing-way.netlify.app/reproducible-research/renv/renv-package.html). 




