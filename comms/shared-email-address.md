# open@win email address

We have a shared email address which is the primary address for users to send queries and can be answered by Open WIN Ambassadors. This address can also be used for external accounts (e.g. mastodon) which Ambassadors manage.

## Shared account set up

### NDCN alias

WIN is not part of the University's official list of departments and centres, so the automated tools for email address management cannot be used. To work around this, we have an alias `win-open@ndcn.ox.ac.uk` which picks up messages sent to `open@win.ox.ac.uk`. We cannot send messages from `open@win.ox.ac.uk`, we can only send from `win-open@ndcn.ox.ac.uk`.

### Auto forwarding

Emails sent to `open@win.ox.ac.uk` are automatically forwarded to a specified number of people. This setting is managed by the account owner (currently Cass) at [https://register.it.ox.ac.uk/self/nexus](https://register.it.ox.ac.uk/self/nexus). This means messages sent to `open@win.ox.ac.uk` will automatically appear in the inboxes of all those on the forward list. This allows access to the shared account on all third party email apps which have access to that persons primary address. Without autoforwarding, access to shared accounts is restricted to Microsoft products.

## Usage

### Who has access

All current Ambassadors will be put on the forward list. At the end of your term, you can choose to remain on the list, or be removed.

Please consider creating a rule for your own mailbox to sweep these messages to a seperate folder if you do not want them to appear in your usual inbox.

Currently:
- Bernd Taschler
- Verena Sarrazin
- Yingshi Feng
- Juju Fars
- Lisa Spiering
- Peter Doohan
- Mohamed Tachrount
- Miguel Farinha

### Who should respond to messages

Anyone who feels able to respond should do so! If you would like to have a conversation about the email before responding, please do so on the Open WIN slack channel `#open-at-win emails` or the Ambassadors' channel.

### How to respond to messages

Please reply to emails from your own address, but cc `open@win.ox.ac.uk`. This way we can be confident that an enquiry has been addressed and all learn from the conversation.
