# Monday Messages
*This file contains draft, upcoming, and previously sent Monday messages about open science topics at WIN.*

---



## Previously Sent Messages

**Questions about data sharing, method sharing or anything open science?**
- Your open WIN ambassadors are here to help. Reach out to the open WIN [email](mailto:open@win.ox.ac.uk) with any questions! For more information about us, see our Open WIN community pages [here](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/)

**What tools can I use share my data with callaborators, reviewers or the world?**
- [Open Science Foundation (OSF)](https://osf.io/): has built-in version control, customisable access links and integration with OneDrive, Dropbox and Google Drive to share large data files. Note that projects that require access to be managed on a large scale can be tricky to coordinate in the current version of OSF. 
- [Zenodo](https://zenodo.org/): lets you share all types of research outputs including, datasets, software, videos and reports with an assigned DOI (digital object identifier). You can tightly control the licencing of your data and who has access to it too!
- [Neurovault](https://neurovault.org/): is an easy way to share statistical brain maps (any brain data stored in a 3D NIFTI format). This can be a great place to share group/participant level contrasts to increase the transparency to your publications. But note, data added to NeuroVault can be reused for any purpose, including commercial ones, without mandatory attribution for reuse​. \

For more details and data-sharing options see the [OPEN WIN community pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/how/) or email us at [open@win.ox.ac.uk](mailto:open@win.ox.ac.uk).

**Looking to get started with Git and GitLab?** \
Check out the OPEN WIN community [guide](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/), which includes a range of tutorials from using the command line, setting up your GitLab account, understanding basic Git commands, to creating a repository and making it citable.\

If you have any questions don't estiate to contact you OPEN WIN ambassadors at [open@win.ox.ac.uk](mailto:open@win.ox.ac.uk).

**Want help promoting your research?**
 If you are planning on sharing code or data from a recent project, we would like to hear about it! The Open WIN ambassadors can help promote any open science you do, just contact us at [open@win.ox.ac.uk](mailto:open@win.ox.ac.uk).


**Wondering about best practises for data science?**
[The Turing Way handbook](https://the-turing-way.netlify.app/index.html) contains guides on how to write reproducible and sharable code, how to use version control for your code and data, how to licence you work and much much more. Its a great supplement to the information already available on the [Open WIN commmunity pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/how/)


**What is Conda? and how can it help you make your computational environments reproducible?**
[Conda](https://docs.conda.io/en/latest/) is a an open source package management system and environment management system that lets you easily creat, save, load and switch between computational environments on your local computer. Conda also lets you package your Python environments into .YAML files to ensure that other users can easily reproduce you're environments when you share your code! For more info on how Conda works and what you can do with it [here](https://the-turing-way.netlify.app/reproducible-research/renv/renv-package.html). 

If you have any questions don't estiate to contact you OPEN WIN ambassadors at [open@win.ox.ac.uk](mailto:open@win.ox.ac.uk).


## Upcoming Messages


## Draft Messages
