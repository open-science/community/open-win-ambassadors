# Drafts and Ideas for communication using social media

## Objectives and observations

The work we are doing here is close to the work done previously for the 'open science bite of the week'.
However, because of the format of twitter, the tone must be slightly more casual. 
At least two options exist for this type of communication. Either a puzzling/mysterious statement/poll and then we answer it by a thread of messages. Or a strong statement that incite replies/questions from the users.

If we use links inside our messages, then twitter should automatically convert the URLs into tinyURL. 


## Instructions

To publish on the WIN tiwtter account, you need to send an email to [comms@win.ox.ac.uk](mailto:comms@win.ox.ac.uk)
Use "Open Science message" as email subject and this template as body: 

**Hello, 
I would like a message from the Open Science ambassadors to be published on the WIN twitter account. 
The message is: 
“[insert your message here]”
Let me know if this fine with you. 
All the best, 
[your name]**

Hanna Smyth [hanna.smyth@ndcn.ox.ac.uk](mailto:hanna.smyth@ndcn.ox.ac.uk) and Jacqueline Pumphrey [jacqueline.pumphrey@ndcn.ox.ac.uk](mailto:jacqueline.pumphrey@ndcn.ox.ac.uk) are the recipients of any email sent to the [comms@win.ox.ac.uk](mailto:comms@win.ox.ac.uk) email adress and are alternatively taking care of the twitter account (weekly rotation). After receiving the Open Science request, they will publish the message.  


## First Drafts 

~~**1. Did you know that WIN has several tutorials to teach/orient researchers on producing open science research? Check here: https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/**~~

~~**2 Git, Gitlab or Github?** --thread
2.1 most of you already know github but for those that are not aware, github is a cloud-based source code management and collaboration tool. You can deposit your analysis, software, app or experiment code in a collaborative environment.
2.2 Not only you can collaborate with your colleagues and keep an online backup of your files but you can also control the version for all the files. 
2.3 However, github is only the first step. It is powerful, but to unleash all its possibilities, you need to use Git. 
2.4 A lot of researchers are not using Git yet due to its lack of practical sense. Git can be academic friendly. Or more like "frenemy". It needs to be tamed. 
2.5 Let's just add that Git allows the users to work on a single private copy of a file at a time. Therefore, it is less conflictual or distractive than a shared Google Doc or a Dropbox. 
2.6 Git can also handle different versions of a project. If you are like me and you fear that making changes in one file will impari the whole project. Well you can still go back to the previous version! (when it was working)
2.7 But which text editor should you use? As you can see here: https://docs.github.com/en/get-started/getting-started-with-git/associating-text-editors-with-git?platform=windows, Microsoft Visual Studio and Sublime text are recommended and available on Windows, Mac and Linux (Personally, I use Sublime text). 
2.8 And how to begin? Many tutorials exist online, but we recommend this blogpost of Vassili Kehayas to start with Git: https://neurathsboat.blog/post/git-intro/ and of course our community pages to start on Gitlab: https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/gitlab-tutorials/~~

~~**3 What do you code your experiment with? Python, Matlab, Java, C#/C++ or Unity (maybe Pearl?).** - poll
3.1 At WIN, we invite the researchers to publish their tasks on the WIN Gitlab https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tasks/~~

**4 Which coding language do you use for data analysis? Python, Matlab, R**, Others -poll

**5 Do you share your data: with OSF? with Zenodo? with OpenNeuro?** -poll 
5.1 If you do not know how to share your data, or if it is your first time, WIN has produced numerous pages to guide you. https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/

**6 Sharing tasks and analysis is cool but sharing MR protocols is even cooler!**
6.1Come see the Open MR Protocol webpage WIN has produced. https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/protocols/
