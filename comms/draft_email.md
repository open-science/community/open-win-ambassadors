# Drafts for the poster repository 

*Please add/modify anything you find useful*

# Draft here

Dear WIN members, 

As you probably already know, our aim is to promote Open Science practices. With this in mind, we recently came up with the idea of creating a common repository for all posters produced by WIN members. This will enable WIN research presented at conferences to be preserved, as well as providing a working and reference archive for future posters. 
In any case, this database is not intended to replace the usual poster repositories (OSF, Zenodo...). It is only intended to collect a copy of the research produced by WIN members in one place. 

However, this database can only be filled in by you, the WIN members. 
What do you think of this idea? Please vote in our short 1-question [poll](https://take.supersurvey.com/poll4880157x26064Df6-151) and do not hesitate to contact us if you have any questions or objections. 


Best, 
The Open Science Ambassadors
