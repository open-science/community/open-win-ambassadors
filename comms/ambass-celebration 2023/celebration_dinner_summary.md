### 2023 Ambassadors Celebration Meeting and Dinner


On January 24, 2023, the Wellcome center for integrative neuroimaging (WIN) hosted their Open Science Celebration Seminar & Dinner at St Anne's College. The event provided a platform to recognize the accomplishments of the open science community at WIN and to discuss the organization's core and evolving values around open science.


The seminar brought together a diverse group of open science practitioners and advocates, including the OPEN WIN ambassadors from 2021/2022 and the new ambassadors cohort of 2022/2023. They shared their experiences, lessons learned, and ideas for the future.
The event also featured talks from members of the open science community outside of Oxford, including [Sarah Stewart](https://www.oii.ox.ac.uk/people/profiles/sarah-stewart/) from the Bodleian Libraries, who spoke about Open Scholarship at Oxford, and [Sarah Callaghan](https://researchsupport.admin.ox.ac.uk/people/sarah-callaghan) from the Oxford Research Strategy and Policy Unit, who discussed best practices for different fields in open research. These speakers helped to broaden the discussion and examine how open science at WIN fits in more broadly with the open science community at Oxford.
Additionally, the event was privileged to have [Stuart Clarke](https://www.win.ox.ac.uk/people/stuart-clare), director of operations at WIN, who gave insight into where WIN is going in the future and how open science fits into that vision.


After the lively discussions, attendees enjoyed a delightful dinner where they continued to discuss all things open science and beyond. The event was a great success, offering an opportunity for members of the open science community to come together, celebrate their achievements, and discuss the future of open science at WIN.


<table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse; border:none;">
<tr>
<td style="border:none; padding:0;">
<img src="ambass-celebration-2023-img-amb-dinner.png" alt="photo of the meeting attendees at dinner" width="100%">
</td>
<td style="border:none; padding:0;">
<img src="ambass-celebration-2023-img-st-annes-coffee.png" alt="st-Annes college oxford coffee cup" width="100%">
</td>
</tr>
</table>
