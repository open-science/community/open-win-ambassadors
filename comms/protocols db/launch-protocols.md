Open WIN MR Protocols Database is now live!

The Open WIN MR Protocols database is a custom built repository to store the technical details of your magnetic resonance data collection. All entries are version controlled, and it's easy to share the full record with colleagues or the wider community. This is the first of major deliverable of the WIN Open Neuroimaging Project - built for and by the community, and ready for your contribution! 

Find out more on the Open WIN Community Pages: tinyurl.com/open-win-protocols

image alt text: Open MR Protocols Database helps you achieve reproducibility, transparency and attribution for your methods. Publish your protocol at tinyurl.com/open-win-protocols. Developed by the Open WIN Community.


gif alt text: Search on the Open MR Protolcs database for keywords, download the full reproducuble methods detail.
 